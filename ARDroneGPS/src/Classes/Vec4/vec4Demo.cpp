#include <iostream>
#include "Vec4.cpp"

using namespace std;


int main(){

  Vec4<float> test1(3.5,6.5,7.5,9.6);
  Vec4<float> test2(9.5,6.5,7.5,9.6);
  Vec4<float> test3;
  Vec3<float> vec3Test;

  test2.print();
  test1.print();


  test2.setX(3.1);
  test2.setY(1.2);
  test2.setZ(3.4);
  test2.setA(6.7);

  test1.set(8.3,.33,2.4,7.9);

  test2.print();
  test1.print();

  test1.zero();
  test1.print();

  test3.set(Vec3<float>(2.7,.045,.93), .045);
  test3.print();

  vec3Test = test3.getVec3();
  vec3Test.print();

  test3 += vec3Test;
  test3.print();
  

  //std::cout << test1.getX() << std::endl;
  //std::cout << test2.getX() << std::endl;

 

  return 0;
}