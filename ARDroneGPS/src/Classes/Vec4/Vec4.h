// Vec4 //////////////////////////////////////////////////////////////////
//
// This a templated vector 4 class that is derived from Vec3
//
// Author: Shawn Braune
///////////////////////////////////////////////////////////////////////////////
#ifndef VEC4_H
#define VEC4_H

#include <iostream>
#include "../Vec3/vec3.h"

template <class TYPE> 
class Vec4: public Vec3<TYPE> {
  
  public:
    //Constructors and Deconstructors
    Vec4();
    Vec4(const Vec4<TYPE>&);
    Vec4(const Vec3<TYPE>&);
    Vec4(const Vec3<TYPE>&, const TYPE &aVal);
    Vec4(const TYPE &xVal, const TYPE &yVal, const TYPE &zVal, const TYPE &aVal);
    ~Vec4();

    //Getters and Setters
    void set(const TYPE &xVal, const TYPE &yVal, const TYPE &zVal, const TYPE &aVal);
    void set(const Vec3<TYPE>&, const TYPE &aVal);
    void setA(const TYPE &val);
    TYPE getA() const;
    Vec4<TYPE> getVec4() const;
    Vec3<TYPE> getVec3() const;

    //Helper
    void print();
    void zero();

    //Overload Operators
    Vec4<TYPE> operator+(const Vec4<TYPE> &vec) const;
    Vec4<TYPE> operator=(const Vec4<TYPE> &vec) const;
    Vec4<TYPE> operator-(const Vec4<TYPE> &vec) const;
    Vec4<TYPE> operator*(const Vec4<TYPE> &vec) const;
    Vec4<TYPE> operator/(const Vec4<TYPE> &vec) const;
    Vec4<TYPE> operator+(const TYPE &val) const;
    Vec4<TYPE> operator-(const TYPE &val) const;
    Vec4<TYPE> operator*(const TYPE &val) const;
    Vec4<TYPE> operator/(const TYPE &val) const;
    void operator+=(const Vec4<TYPE> &vec);
    void operator-=(const Vec4<TYPE> &vec);
    void operator*=(const Vec4<TYPE> &vec);
    void operator/=(const Vec4<TYPE> &vec);
    void operator+=(const TYPE &val);
    void operator-=(const TYPE &val);
    void operator*=(const TYPE &val);
    void operator/=(const TYPE &val);



  protected:
    TYPE *a;

};

#endif