#include "Vec4.h"

#include <cstddef>
#include <iostream>

//Contructor //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE>::Vec4() : Vec3<TYPE>::Vec3(){

  a = new TYPE;

  *a = 0.00;
}

//Contructor //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE>::Vec4(const TYPE &xVal, const TYPE &yVal, const TYPE &zVal, const TYPE &aVal)
                 : Vec3<TYPE>::Vec3(xVal,yVal,zVal){
  
  a = new TYPE;

  *a = aVal;
}

//Contructor //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE>::Vec4(const Vec3<TYPE>& vec3) 
                : Vec3<TYPE>::Vec3(vec3.getX(),vec3.getX(),vec3.getX()){

  a = new TYPE;

  *a = 0.0;
}

//Contructor //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE>::Vec4(const Vec3<TYPE>& vec3, const TYPE &aVal) 
                : Vec3<TYPE>::Vec3(vec3.getX(),vec3.getX(),vec3.getX()){

  a = new TYPE;

  *a = aVal;
}

//Copy Contructor /////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE>::Vec4(const Vec4<TYPE>& vec4) 
                : Vec3<TYPE>::Vec3(vec4.getX(),vec4.getX(),vec4.getX()){

  a = new TYPE;

  *a = 0.0;
}

//De-Contructor ///////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE>::~Vec4(){

  delete a;

  a = NULL;

}

// getA() /////////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
TYPE Vec4<TYPE>::getA() const{
  return *a;
}

// set() /////////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::set(const TYPE &xVal, const TYPE &yVal, const TYPE &zVal, const TYPE &aVal){
  *(Vec3<TYPE>::x) = xVal;
  *(Vec3<TYPE>::y) = yVal;
  *(Vec3<TYPE>::z) = zVal;
  *a = aVal;
}

template <class TYPE>
void Vec4<TYPE>::set(const Vec3<TYPE> &vec3, const TYPE &aVal){
  *(Vec3<TYPE>::x) = vec3.getX();
  *(Vec3<TYPE>::y) = vec3.getY();
  *(Vec3<TYPE>::z) = vec3.getZ();
  *a = aVal;
}

// setA() /////////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::setA(const TYPE &val){
  *a = val;
}

// print() /////////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::print(){
  std::cout << "x = " << *(Vec3<TYPE>::x)
            << " y = " << *(Vec3<TYPE>::y)
            << " z = " << *(Vec3<TYPE>::z)
            << " a = " << *a << "\n";
}

// zero() /////////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::zero(){
  Vec3<TYPE>::zero();
  *a = 0.00;

}

template <class TYPE>
Vec3<TYPE> Vec4<TYPE>::getVec3() const{

  return Vec3<TYPE>(*(Vec3<TYPE>::x),*(Vec3<TYPE>::y),*(Vec3<TYPE>::z));
}

template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::getVec4() const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x),*(Vec3<TYPE>::y),*(Vec3<TYPE>::z), *a);
}

// operator+ //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator+(const Vec4<TYPE> &vec) const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x) + vec.getX(),
                    *(Vec3<TYPE>::y) + vec.getY(),
                    *(Vec3<TYPE>::z) + vec.getZ(),
                    *a + vec.getA());
}

// operator+ //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator+(const TYPE &val) const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x) + val,
                    *(Vec3<TYPE>::y) + val,
                    *(Vec3<TYPE>::z) + val,
                    *a + val);
}

// operator- //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator-(const Vec4<TYPE> &vec) const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x) - vec.getX(),
                    *(Vec3<TYPE>::y) - vec.getY(),
                    *(Vec3<TYPE>::z) - vec.getZ(),
                    *a - vec.getA());
}

// operator- //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator-(const TYPE &val) const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x) - val,
                    *(Vec3<TYPE>::y) - val,
                    *(Vec3<TYPE>::z) - val,
                    *a - val);
}

// operator* //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator*(const Vec4<TYPE> &vec) const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x) * vec.getX(),
                    *(Vec3<TYPE>::y) * vec.getY(),
                    *(Vec3<TYPE>::z) * vec.getZ(),
                    *a * vec.getA());
}

// operator* //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator*(const TYPE &val) const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x) * val,
                    *(Vec3<TYPE>::y) * val,
                    *(Vec3<TYPE>::z) * val,
                    *a * val);
}

// operator/ //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator/(const Vec4<TYPE> &vec) const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x) / vec.getX(),
                    *(Vec3<TYPE>::y) / vec.getY(),
                    *(Vec3<TYPE>::z) / vec.getZ(),
                    *a / vec.getA());
}

// operator/ //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator/(const TYPE &val) const{

  return Vec4<TYPE>(*(Vec3<TYPE>::x) / val,
                    *(Vec3<TYPE>::y) / val,
                    *(Vec3<TYPE>::z) / val,
                    *a / val);
}

// operator+= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::operator+=(const Vec4 &vec){
  *(Vec3<TYPE>::x) += vec.getX();
  *(Vec3<TYPE>::y) += vec.getY();
  *(Vec3<TYPE>::z) += vec.getZ();
  *a += vec.getA();
}

// operator+= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::operator+=(const TYPE &val){
  *(Vec3<TYPE>::x) += val;
  *(Vec3<TYPE>::y) += val;
  *(Vec3<TYPE>::z) += val;
  *a += val;
}

// operator-= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::operator-=(const Vec4 &vec){
  *(Vec3<TYPE>::x) -= vec.getX();
  *(Vec3<TYPE>::y) -= vec.getY();
  *(Vec3<TYPE>::z) -= vec.getZ();
  *a -= vec.getA();
}

// operator-= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::operator-=(const TYPE &val){
  *(Vec3<TYPE>::x) -= val;
  *(Vec3<TYPE>::y) -= val;
  *(Vec3<TYPE>::z) -= val;
  *a -= val;
}

// operator*= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::operator*=(const Vec4 &vec){
  *(Vec3<TYPE>::x) *= vec.getX();
  *(Vec3<TYPE>::y) *= vec.getY();
  *(Vec3<TYPE>::z) *= vec.getZ();
  *a *= vec.getA();
}

// operator*= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::operator*=(const TYPE &val){
  *(Vec3<TYPE>::x) *= val;
  *(Vec3<TYPE>::y) *= val;
  *(Vec3<TYPE>::z) *= val;
  *a *= val;
}

// operator/= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::operator/=(const Vec4 &vec){
  *(Vec3<TYPE>::x) /= vec.getX();
  *(Vec3<TYPE>::y) /= vec.getY();
  *(Vec3<TYPE>::z) /= vec.getZ();
  *a /= vec.getA();
}

// operator/= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
void Vec4<TYPE>::operator/=(const TYPE &val){
  *(Vec3<TYPE>::x) /= val;
  *(Vec3<TYPE>::y) /= val;
  *(Vec3<TYPE>::z) /= val;
  *a /= val;
}

// operator= //////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template <class TYPE>
Vec4<TYPE> Vec4<TYPE>::operator=(const Vec4<TYPE> &vec) const{

  if(this != &vec){
     *(Vec3<TYPE>::x) = vec.getX();
     *(Vec3<TYPE>::y) = vec.getY();
     *(Vec3<TYPE>::z) = vec.getZ();
    *a = vec.getA();

  }

  return *this;
}

template class Vec4<float>;