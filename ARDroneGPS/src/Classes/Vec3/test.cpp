#include <iostream>
#include "vec3.cpp"

using namespace std;


int main(){

  Vec3<float> test1;
  Vec3<float> test2(3.56,2.34,5.2);
  Vec3<float> test4(test2);
  Vec3<float> test3, test5, test6, test7,test8;

  

  cout << "Test Vec1: " << test1.getX() << " " << test1.getY() << " " << test1.getZ() << "\n";

  test2.setX(1.34f);

  //testing getters
  cout << "Test Vec2: " << test2.getX() << " " << test2.getY() << " " << test2.getZ() << "\n";
  cout << "Test Vec4: "<< test4.getX() << " " << test4.getY() << " " << test4.getZ() << "\n";


  //testing Setters
  test1.set(5.78, 3.56, 2.34);
  cout << "Setter Test Vec1: " << test1.getX() << " " << test1.getY() << " " << test1.getZ() << "\n";

  //adding
  test3 = test1 + test2;
  cout << "Test Addition Vec3: " << test3.getX() << " " << test3.getY() << " " << test3.getZ() << "\n";

  //subtracting
  test5 = test1 - test2;
  cout << "Test subtracting Vec5: " << test5.getX() << " " << test5.getY() << " " << test5.getZ() << "\n";


  //multiplication
  test6 = test1 * test2;
  cout << "Test multiplication Vec6: " << test6.getX() << " " << test6.getY() << " " << test6.getZ() << "\n";


  //divison
  test7 = test1 / test2;
  cout << "Test subtracting Vec7: " << test7.getX() << " " << test7.getY() << " " << test7.getZ() << "\n";

  //Scaler

  //addtion
  test3 = test3 + 4.0;
  cout << "Test Addition Vec3: " << test3.getX() << " " << test3.getY() << " " << test3.getZ() << "\n";

//subtracting
  test5 = test5 - 2.5;
  cout << "Test subtracting Vec5: " << test5.getX() << " " << test5.getY() << " " << test5.getZ() << "\n";


  //multiplication
  test6 = test6 * 3.0;
  cout << "Test multiplication Vec6: " << test6.getX() << " " << test6.getY() << " " << test6.getZ() << "\n";


  //divison
  test7 = test7 / 2.0;
  cout << "Test dividing Vec7: " << test7.getX() << " " << test7.getY() << " " << test7.getZ() << "\n";

test3.zero();
test5.zero();
//test6.zero();
//test7.zero();

//testing plus equals
  //addtion
  test3 += test1;
  cout << "Test Addition Vec3: " << test3.getX() << " " << test3.getY() << " " << test3.getZ() << "\n";

//subtracting
  test5 -= test5 + 2;
  cout << "Test subtracting Vec5: " << test5.getX() << " " << test5.getY() << " " << test5.getZ() << "\n";


  //multiplication
  test6 *= test1;
  cout << "Test multiplication Vec6: " << test6.getX() << " " << test6.getY() << " " << test6.getZ() << "\n";


  //divison
  test7 /= test2;
  cout << "Test dividing Vec7: " << test7.getX() << " " << test7.getY() << " " << test7.getZ() << "\n";


    //addtion
  test7 += 10.5;
  cout << "Test Addition Vec3: " << test7.getX() << " " << test7.getY() << " " << test7.getZ() << "\n";

//subtracting
  test5 -= 5.0;
  cout << "Test subtracting Vec5: " << test5.getX() << " " << test5.getY() << " " << test5.getZ() << "\n";


  //multiplication
  test3 *= 2.56;
  cout << "Test multiplication Vec6: " << test3.getX() << " " << test3.getY() << " " << test3.getZ() << "\n";


  //divison
  test6 /= 3.65;
  cout << "Test dividing Vec7: " << test6.getX() << " " << test6.getY() << " " << test6.getZ() << "\n";

 // cout << test6;

  return 0;
}