/**
 @class     Vec3
 @brief     Vec3 Templated Class for UAV Systems
 @details   This class is a templated Vec3 class tht is used in conjuction with 
            UAV Systems
 @author    Shawn Braune
 @version   1.0
 @date      2014
 @pre       First initialize the system.

 @warning   Improper use can crash your application
 */
#ifndef VEC3_H
#define VEC3_H

#include <iostream>

template <class TYPE> 
class Vec3 {
  
  public:

    /** @brief Default Constructor

    This is the Default Constructor for the Vec3 class. It initializes and sets
    all values to zero.

    @author Shawn Braune
    @date October 2014
    */
    Vec3();

    /** @brief Copy Constructor

    This is the Copy Consstructor for passing Vec3 by value or to make a copy of
    a Vec3. 

    @param Vec3<TYPE> Vector 3 to be copied and *Must Be of Vec3 Type*

    @author Shawn Braune
    @date October 2014
    */
    Vec3(const Vec3<TYPE>&);

    /** @brief Constructor

    This Constructor can be used to intialize a contrsuctor with TYPE values. 

    @param TYPE Value for X and *Must Be of Vec3 Type*
    @param TYPE Value for Y and *Must Be of Vec3 Type*
    @param TYPE Value for Z and *Must Be of Vec3 Type*

    @author Shawn Braune
    @date October 2014
    */
    Vec3(const TYPE &xVal, const TYPE &yVal, const TYPE &zVal);

    /** @brief Destructor

    Dealocates memory and sets value to NULL

    @author Shawn Braune
    @date October 2014
    */
    ~Vec3();

    //Getters and Setters
    /** @brief Set X,Y and Z Values

    This sets the X,Y and Z cordinates respectivly. 

    @param TYPE Value for X and *Must Be of Vec3 Type*
    @param TYPE Value for Y and *Must Be of Vec3 Type*
    @param TYPE Value for Z and *Must Be of Vec3 Type*

    @see setX()
    @see setY()
    @see setZ()

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;

     TYPE xVal = 3.5,
          yVal = 2.2,
          zVal = .95;

     vec.set(xval,yval,zval);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    virtual void set(const TYPE &xVal, const TYPE &yVal, const TYPE &zVal);

    /** @brief Set X Value

    This sets the X value respectivly. 

    @param TYPE Value for X and *Must Be of Vec3 Type*

    @see set()
    @see setY()
    @see setZ()

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;

     TYPE xVal = 3.5;

     vec.setX(xval);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void setX(const TYPE &val);

    /** @brief Set Y Value

    This sets the Y value respectivly. 

    @param TYPE Value for Y and *Must Be of Vec3 Type*

    @see set()
    @see setX()
    @see setZ()

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;

     TYPE yVal = 2.2;

     vec.setY(yval);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void setY(const TYPE &val);

    /** @brief Set Z Value

    This sets the Z value respectivly. 

    @param TYPE Value for Z and *Must Be of Vec3 Type*

    @see set()
    @see setX()
    @see setY()

    @b Examples
    @code
     //Example using float Type

     Vec3<float> vec;

     float zVal = .95;

     vec.setZ(zVal);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void setZ(const TYPE &val);

    /** @brief Get X

    This Gets the X value and returns the value of TYPE Vec3<TYPE>. 

    @returns TYPE Value for X

    @see getY()
    @see getZ()

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;

     float xVal;

     xVal = vec.getX();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    TYPE getX() const;

    /** @brief Get X

    This Gets the X value and returns the value of TYPE Vec3<TYPE>. 

    @returns TYPE Value for X

    @see getX()
    @see getZ()

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;

     float yVal;

     yVal = vec.getY();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    TYPE getY() const;

    /** @brief Get Z

    This Gets the Z value and returns the value of TYPE Vec3<TYPE>. 

    @returns TYPE Value for Z

    @see getX()
    @see getY()

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;

     float zVal;

     zVal = vec.getZ();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    TYPE getZ() const;

    //Helper
    /** @brief Zero

    A helper function that sets the vector to zero

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;

     vec.zero();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    virtual void zero();

    /** @brief Print

    A helper function that prints the vector to screen

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;

     vec.print();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    virtual void print();

    //Overload Operators
    /** @brief Addition of two Vec3

    This is an overloaded addition operator that takes two Vec3<TYPE> and 
    adds them together.

    @returns Vec3<TYPE> Added Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2,vec3;

     vec3 = vec1 + vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec3<TYPE> operator+(const Vec3<TYPE> &vec) const;

    /** @brief Subtraction of two Vec3

    This is an overloaded addition operator that takes two Vec3<TYPE> and 
    subtractions them.

    @returns Vec3<TYPE> Subtracted Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2,vec3;

     vec3 = vec1 - vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec3<TYPE> operator-(const Vec3<TYPE> &vec) const;

    /** @brief Multiplication of two Vec3

    This is an overloaded subtraction operator that takes two Vec3<TYPE> and 
    multiplies them together.

    @returns Vec3<TYPE> Multiplied Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2,vec3;

     vec3 = vec1 * vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec3<TYPE> operator*(const Vec3<TYPE> &vec) const;

    /** @brief Division of two Vec3

    This is an overloaded multiplication operator that takes two Vec3<TYPE> and 
    divides them.

    @returns Vec3<TYPE> Divided Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2,vec3;

     vec3 = vec1 / vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec3<TYPE> operator/(const Vec3<TYPE> &vec) const;

    /** @brief Addition of a Vec3 and Scaler

    This is an overloaded divide operator that takes a Vec3<TYPE> and 
     a scaler and adds them together respectivily.

    @returns Vec3<TYPE> Added Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec2 = vec1 + 2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec3<TYPE> operator+(const TYPE &val) const;

    /** @brief Subtraction of a Vec3 and Scaler

    This is an overloaded addition operator that takes a Vec3<TYPE> and 
     a scaler and subtracts them.

    @returns Vec3<TYPE> Subtracts Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec2 = vec1 - 2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    Vec3<TYPE> operator-(const TYPE &val) const;

    /** @brief Multiplication of a Vec3 and Scaler

    This is an overloaded subtraction operator that takes a Vec3<TYPE> and 
     a scaler and multiplies them together respectivily.

    @returns Vec3<TYPE> Added Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec2 = vec1 * 2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec3<TYPE> operator*(const TYPE &val) const;

    /** @brief Division of a Vec3 and Scaler

    This is an overloaded multiplication operator that takes a Vec3<TYPE> and 
     a scaler and divdes them.

    @returns Vec3<TYPE> Added Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec2 = vec1 / 2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec3<TYPE> operator/(const TYPE &val) const;

    /** @brief Addition Equals

    This is an overloaded divde operator that takes a Vec3<TYPE> and
    adds them to Vec3 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 += vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void operator+=(const Vec3<TYPE> &vec);

    /** @brief Subtraction Equals

    This is an overloaded addition equals operator that takes a Vec3<TYPE> and
    subtracts them from the Vec3 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 -= vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void operator-=(const Vec3<TYPE> &vec);

    /** @brief Multiply Equals

    This is an overloaded subtraction equals operator that takes a Vec3<TYPE> and
    multiplies with the Vec3 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 *= vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    void operator*=(const Vec3<TYPE> &vec);

    /** @brief Addition Equals

    This is an overloaded multiplication equals operator that takes a Vec3<TYPE> and
    divdes it from the Vec3 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 /= vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    void operator/=(const Vec3<TYPE> &vec);

    /** @brief Addition Equals with Scaler

    This is an overloaded divide equals operator that takes a scaler and
    adds it to Vec3 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 += 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void operator+=(const TYPE &val);

    /** @brief Subtraction Equals with Scaler

    This is an overloaded subtraction equals operator that takes a scaler and
    adds it to Vec3 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 -= 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    void operator-=(const TYPE &val);

    /** @brief Multiplication Equals with Scaler

    This is an overloaded multiplication equals operator that takes a scaler and
    adds it to Vec3 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 *= 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    void operator*=(const TYPE &val);

    /** @brief Divide Equals with Scaler

    This is an overloaded divide equals operator that takes a scaler and
    adds it to Vec3 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 /= 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    void operator/=(const TYPE &val);

    /** @brief Equals

    This is an overloaded equals operator that takes a Vec3<TYPE>
    on the left side and copies it to the Vec3<TYPE> on the left side.

    @returns Vec3<TYPE> Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1 -= 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    Vec3<TYPE> operator=(const Vec3<TYPE> &vec) const;

  protected:

    TYPE *x, /**< TYPE pointer X */ 

         *y, /**< TYPE pointer Y */ 

         *z; /**< TYPE pointer Z */ 

};

#endif