/** @brief ARDroneGPS
  
  This is the node that recieves data from the Vivon and will publish
  it to it's own topic

  After Compiling you need to run the following:
    rosrun ARDroneGPS ARDroneGPS _looprate=:looprate

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include </home/tamucc/sbraune/UAV/vicon/msg_gen/cpp/include/vicon/position.h>
#include "./Classes/Vec4/Vec4.h"
#include <geometry_msgs/Twist.h>

int count = 0;

ros::Publisher UAV_1_Position, UAV_2_Position, UAV_3_Position, Rovio_1_Position;


void publishViconData(ros::Publisher *const publisher, Vec3<float> rot, Vec3<float> trans);

/** @brief getViconData
  This function subscribes to the Vicon Topic and publishes the Vicon Data

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
void getViconData(const vicon::position& msg)
{
  ROS_INFO("UAV 1:");
  ROS_INFO("X=[%f],Y=[%f],Z=[%f]",msg.x,msg.y,msg.z);
  ROS_INFO("Roll=[%f],Pitch=[%f],Yaw=[%f]",msg.roll,msg.pitch,msg.yaw);
  ROS_INFO("Emer=[%d]\n",msg.Emer);

  publishViconData(&UAV_1_Position, Vec3<float>(msg.x,msg.y,msg.z), Vec3<float>(msg.roll,msg.pitch,msg.yaw));

  ROS_INFO("UAV 2:");
  ROS_INFO("X=[%f],Y=[%f],Z=[%f]",msg.ux,msg.uy,msg.uz);
  ROS_INFO("Roll=[%f],Pitch=[%f],Yaw=[%f]\n",msg.uroll,msg.upitch,msg.uyaw);

  publishViconData(&UAV_2_Position, Vec3<float>(msg.ux,msg.uy,msg.uz), Vec3<float>(msg.uroll,msg.upitch,msg.uyaw));
  

  ROS_INFO("UAV 3:");
  ROS_INFO("X=[%f],Y=[%f],Z=[%f]",msg.UAV_3_x,msg.UAV_3_y,msg.UAV_3_z);
  ROS_INFO("Roll=[%f],Pitch=[%f],Yaw=[%f]\n",msg.UAV_3_roll,msg.UAV_3_pitch,msg.UAV_3_yaw);

  publishViconData(&UAV_3_Position, Vec3<float>(msg.UAV_3_x,msg.UAV_3_y,msg.UAV_3_z), Vec3<float>(msg.UAV_3_roll,msg.UAV_3_pitch,msg.UAV_3_yaw));


  ROS_INFO("Rovio 1:");
  ROS_INFO("X=[%f],Y=[%f],Z=[%f]",msg.mx,msg.my,msg.mz);
  ROS_INFO("Roll=[%f],Pitch=[%f],Yaw=[%f]\n",msg.mroll,msg.mpitch,msg.myaw);

  publishViconData(&Rovio_1_Position, Vec3<float>(msg.mx,msg.my,msg.mz), Vec3<float>(msg.mroll,msg.mpitch,msg.myaw));

}



int main(int argc, char **argv){

  ros::init(argc, argv, "ARDroneGPS");

  ros::NodeHandle node;
  int looprate;

  //creating Publisher
  ros::Publisher pub = node.advertise<std_msgs::String>("/GlassNode/cmd", 1000);

  UAV_1_Position = node.advertise<geometry_msgs::Twist>("/ARDroneGPS/UAV1/Position", 1000);
  UAV_2_Position = node.advertise<geometry_msgs::Twist>("/ARDroneGPS/UAV2/Position", 1000);
  //UAV_3_Position = node.advertise<geometry_msgs::Twist>("/ARDroneGPS/UAV3/Position", 1000);
  //Rovio_1_Position = node.advertise<geometry_msgs::Twist>("/ARDroneGPS/Rovio1/Position", 1000);

  ros::Subscriber vicon = node.subscribe("position", 1000, getViconData); // read Vicon

  ros::NodeHandle private_node_handle_("~");

  private_node_handle_.param("looprate", looprate, int(50));
 
  ROS_INFO("Looprate: %d", looprate);

  ros::Rate loop_rate(looprate);

  while(ros::ok()){
   // publish(pub);
    ros::spinOnce();
    loop_rate.sleep();
    count++;
  }

  return 0;
}

/** @brief Publish the Vicon Data
  This function provides a means to publish the information given by the 
  Vicon of the UAV to it's own seperate topic.

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
void publishViconData(ros::Publisher *const publisher, Vec3<float> pos, Vec3<float> orient){

  geometry_msgs::Twist msg;
  msg.linear.x = pos.getX(); //X
  msg.linear.y = pos.getY(); //Y
  msg.linear.z = pos.getZ(); //Z

  msg.angular.x = orient.getX(); //Roll
  msg.angular.y = orient.getY(); //Pitch
  msg.angular.z = orient.getZ(); //Yaw

  publisher->publish(msg);
}
