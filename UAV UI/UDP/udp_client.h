/**
 @class     UDP_Client
 @brief     UDP Client for Coms
 @details   This is the class for use by the client in UDP
            communication
 @author    Shawn Braune
 @version   1.0
 @date      April 2015

 @warning   Improper use can crash your application and there 
            are no mechanisms in place for errors
 */
#ifndef UDP_CLIENT_H
#define UDP_CLIENT_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <arpa/inet.h>
#include <iostream>

class UDP_Client{

  public:
    /** @brief Default Constructor

    This is the Default Constructor for the UDP_Client Class

    @author Shawn Braune
    @date   April 2015
    */
    UDP_Client();
    /** @brief Constructor

    This is the a Constructor for the UDP_Client Class

    @params std::string The IPAddress of the Sever you are
                        going to communicate with.

    @params int port number of the server you want to 
                communicate with

    @author Shawn Braune
    @date Feb 2015
    */
    UDP_Client(std::string ipAddr, int port);

    /** @brief deconstructor

    This is the deconstructor for the UDP_Client Class

    @author Shawn Braune
    @date   April 2015
    */
    ~UDP_Client();

    /** @brief Send Message to Server

    This is allows you to send a message to the Server
    the user has chosen

    @params unsigned char* A Pointer ot the unsigned Char Buffer
    @params int Size of the buffer 

    @author Shawn Braune
    @date April 2015
    */
    void send_Message(unsigned char*, int);

  private:
    
    int sock; /**Socket*/
    struct sockaddr_in server, /**Struct of Server*/
                       client; /**Struct of Client*/
    socklen_t server_len,      /**Struct for Sever length*/
              client_len;      /**Struct for Client length*/

};
#endif