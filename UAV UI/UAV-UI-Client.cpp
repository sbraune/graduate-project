/** @brief UAV Mock User Interface - Command
  
  This is a mockup of how to create the User Interface
  to allow a user to send a commands to the Glass node

  @warning you will have to change the IP

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
#include <iostream>
#include <stdio.h>
#include <string.h>
#include "UDP/udp_client.h"
using namespace std;

const int NUM_UAVS = 2;
const int NUM_ACTIONS = 6;
const int NUM_ALPHA = 5;

//Constants for UAVs
string UAV[3] = {"NULL","KR1", "KR2"};
string UAV_ACTIONS[7] = {"NULL","TAKEOFF", "LAND", "HOVER", "GOTO", "PATTERN", "RESET"};
string UAV_ALPHA[6] = {"NULL","ALPHA", "BETA", "GAMMA", "DELTA", "EPSILON"};

int main (int argc,char **argv){

  UDP_Client client("192.168.2.4");

	cout << argc << "\n";
	int temp_cmd[6] = {0,0,0,0,0,0};
	unsigned buffer[12]; //Buffer to send

	//Get UAV ID
	for(int i = 0; i < NUM_UAVS; i++){
		if(argv[1] == UAV[i+1]){
			//cout << i << " ";
			temp_cmd[0] = i+1;
		}
	}

	//Check if UAV exists
	if(temp_cmd[0] == 0){
		cout << "UAV does not exist!!\n";
		return 1;
	}

	//Get Primary command
	for(int i = 0; i < NUM_ACTIONS; i++){
		if(argv[2] == UAV_ACTIONS[i+1]){
		//	cout << i << " ";
			temp_cmd[1] = i+1;
		}
	}

    //Check if Primary Command exists
	if(temp_cmd[1] == 0){
		cout << "Command does not exist!!\n";
		return 1;
	}


    //Get Alpha

    if(argc > 3){
	for(int i = 0; i < NUM_ALPHA; i++){
		if(argv[3] == UAV_ALPHA[i+1]){
		//	cout << i << " ";
			temp_cmd[2] = i+1;
		}
	}

	//Check if ALpha exists
	if(temp_cmd[2] == 0){
		cout << "Primary Alpha does not exist!!\n";
		return 1;
	}}

  /*
	//Get Secondary Command
	if(argv[4] == UAV_ACTIONS[5]){ //AND CMD
	  temp_cmd[3] = 5;

	  //cout << 4 << " ";

	  for(int i = 1; i < 4; i++){
		if(argv[5] == UAV_ACTIONS[i+1]){
			//cout << i << " ";
			temp_cmd[4] = i+1;
	  }
	}

	}
	else if(argv[4] == UAV_ACTIONS[6]){ //Pattern CMD
	  temp_cmd[3] = 6;
	  //cout << 5 << " ";
	  for(int i = 0; i < NUM_ALPHA; i++){
		  if(argv[5] == UAV_ALPHA[i+1]){
		  	//cout << i << " ";
			  temp_cmd[4] = i+1;
	    }
	  }
  }*/
 

  //calculate Checksum
  temp_cmd[5] = temp_cmd[0] + temp_cmd[1] + temp_cmd[2] + 
              temp_cmd[3] + temp_cmd[4];

  //print the command
  for(int i = 0; i < 6; i++){
     cout << temp_cmd[i] << " ";
  }
  cout << "\n";

  //Split command and place in buffer
  for(int i = 0; i < 6; i++){
	  buffer[i*2] = temp_cmd[i] &0xFF;
	  buffer[(i*2)+1] = (temp_cmd[i] >> 8) &0xFF;
  }
  
  //Send Command
  client.send_Message(buffer,12);

	return 0;
}