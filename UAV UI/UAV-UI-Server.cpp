/** @brief UAV Mock User Interface - Server
  
  This is a mockup of how to create the User Interface
  to allow a user to recieve a data from the Glass node

  @warning Saturation will need to be changed based on
           room size. 

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "UDP/udp_server.h"
using namespace std;

const int sat = 20000; //saturation 20 meters

int main()
{
  int data[10], uav[2][8], checksum;
  UDP_Server server(54000);
  unsigned char buf[20];

 while(true){
    server.rcv_Message(buf, 20);
    system("clear");

    //print data coming in, can be taken out
    std::cout << (buf[0] | buf[1] << 8) << " ";
    std::cout << (buf[2] | buf[3] << 8) - sat << " ";
    std::cout << (buf[4] | buf[5] << 8) - sat << " ";
    std::cout << (buf[6] | buf[7] << 8) - sat << " ";
    std::cout << (buf[8] | buf[9] << 8) - sat << " ";
    std::cout << (buf[10] | buf[11] << 8) - sat << " ";
    std::cout << (buf[12] | buf[13] << 8) - sat << " ";
    std::cout << (buf[14] | buf[15] << 8) << " ";
    std::cout << (buf[16] | buf[17] << 8) << " ";
    std::cout << (buf[18] | buf[19] << 8) << "\n";

    //place data in array
    data[0] =(buf[0] | buf[1] << 8);
    data[1] =(buf[2] | buf[3] << 8) - sat;
    data[2] =(buf[4] | buf[5] << 8) - sat;
    data[3] =(buf[6] | buf[7] << 8) - sat;
    data[4] =(buf[8] | buf[9] << 8) - sat;
    data[5] =(buf[10] | buf[11] << 8) - sat;
    data[6] =(buf[12] | buf[13] << 8) - sat;
    data[7] =(buf[14] | buf[15] << 8);
    data[8] =(buf[16] | buf[17] << 8);
    data[9] =(buf[18] | buf[19] << 8);
    
    //calculate checksum
    checksum = data[0] + fabs(data[1]) + fabs(data[2]) + fabs(data[3]) + 
               fabs(data[4]) + fabs(data[5]) + fabs(data[6]) + data[7] + data[8];
    
    //printing checksum for confirmation
    cout << "Checksum Computer: " << checksum << " RCV Checksum: " << data[9] << "\n";
    
    //if Checksum is correct update in UAv array
    if(checksum == data[9]){
        int u = data[0];
        //cout << "test\n";
        for(int i=0; i < 9; i++){
          uav[u-1][i] = data[i+1];
            cout << uav[u-1][i] << " ";
        }
        cout << "\n";
    }

    //Print to user in desired fashion
    cout << "UAV  : 1         "                << "UAV  : 2\n" 
         << "X    : " << uav[0][0] << "      " << "X    : " << uav[1][0] << "\n"
         << "Y    : " << uav[0][1] << "      " << "Y    : " << uav[1][1]<<  "\n"
         << "Z    : " << uav[0][2] << "      " << "Z    : " << uav[1][2]<< "\n"
         << "Pitch: " << uav[0][3] << "      " << "Pitch: "<< uav[1][3]<< "\n"
         << "Roll : " << uav[0][4] << "      " << "Roll : "<< uav[1][4]<< "\n"
         << "Yaw  : " << uav[0][5] << "      " << "Yaw  : "<< uav[1][5]<< "\n"
         << "Fuel : " << uav[0][6] << "      " << "Fuel : "<< uav[1][6]<< "\n"
         << "State: " << uav[0][7] << "      " << "State: "<< uav[1][7]<< "\n"; 
 }
}
