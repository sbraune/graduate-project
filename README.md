# README #

This is the software for my Graduate Project for my Masters of Science in Computer Science. It is a prototype for a design on how to implement a Search and Rescue effort. The idea is to allow the user the ability to control a UAV from any sort of medium, mainly Head Mounted Display such as the google glass. The UI gives an idea on how to send a receive the data.

### What is this repository for? ###

* Is to allow students and researches with a backbone on how to control UAVs indoors. The code can be translated to out door use but will need some changes.
* Version 1.0
* Author - Shawn Braune
* Developed at Texas A&M at Corpus Christi 

### How do I get set up? ###

* Dependencies
  * Language
    * The language is C++ and is in pure object oriented style. All the classes are made to work with one another.
  * ROS (http://www.ros.org/)
    *ROS Dependencies
      * ROS Dependencies 
      * roscpp
      * sensor_msgs
      * std_msgs
      * std_srvs
      * tf
  * This uses ROS Fuerte. The issue is Fuerte is Deprecated and should not be used anymore. The code can easily be updated to work with the latest version of ROS but there might need to be some changes.
  * VRPN (http://wiki.ros.org/ros_vrpn_client)
    * This is the medium in which the data is gathered by the ARDrone and picked up by the ARDroneGPS node. The one included in the repo is for Fuerte but it should be updated when ROS is.
  * ARDrone Autonomy Driver (https://github.com/AutonomyLab/ardrone_autonomy)
    * This is the main driver to control the ARDrone. In tests it has been shown to be unreliable. Though this is the Fuerte driver and the newer one probably is better it might be a better choice to develop your own ARDrone Driver if using the ARDrone2. One of the major flaws in the driver is the lack of multi UAV support without hacking the code

* Configuration
  * Please refer to the source code and ROS website for information on how to configure the software

* How to run tests

    roslaunch UAVGradProj Vicon.launch
    roslaunch UAVGradProj Drone1.launch
    roslaunch UAVGradProj Drone2.launch
    rosrun ARDroneGPS ARDroneGPS
    rosrun GlassNode Glassnode
    rosrun UAVGradProject UAVGradProj _logfile=:"Logfile Path" _looprate=:looprate

### Usage guidelines ###

* Before using please ask for permission. In many cases it will be granted but please ask before using.
* Please give credit where credit is due by citing the code and the authors name. 
* Have fun!!!

### Who do I talk to? ###

* You can message me for questions about the code
* Dr. Luis Garcia at TAMUCC