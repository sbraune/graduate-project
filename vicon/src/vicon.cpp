/*****************************************
David Isaias Montufar Aguilar and Christoph Hintz
Programa para convertir los valores de cuaterniones a Angulos de euler de los valores recividos 
por las camaras Vicon.

Dependecias: roscpp tf sensor_msgs
Programa modificado 04/05/2014
*/

#include "ros/ros.h"
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include "tf/tfMessage.h"

#include </home/tamucc/sbraune/UAV/vicon/msg_gen/cpp/include/vicon/position.h>

#define _USE_MATH_DEFINES 
#include "math.h"
#include <string.h>

    double  px[4] = {0,0,0};
    double  py[4] = {0,0,0};
    double  pz[4] = {0,0,0};
    double  proll[4] = {0,0,0};
    double  ppitch[4] = {0,0,0};
    double  pyaw[4] = {0,0,0};
    bool Emergency[4]={false,false, false, false};

int main(int argc,char **argv)
{
    ros::init(argc,argv, "Vicon");
    ros::NodeHandle node;    
   
    tf::TransformListener listener;
    ros::Publisher pub = node.advertise<vicon::position>("position", 1000); 	//publicamos posicion y velocidad

    ros::Rate rate(50);

    while(node.ok())
    {
        tf::StampedTransform transform;

        try{
            listener.lookupTransform("/Vicon","/ARDrone",ros::Time(0), transform);

      //Transformations
            double tx1 = transform.getOrigin().x();
            double ty1 = transform.getOrigin().y();
            double tz1 = transform.getOrigin().z();

            tf::Quaternion quat = transform.getRotation();
            double roll1, pitch1, yaw1;	   
//            tf::Matrix3x3(quat);
            tf::Matrix3x3(quat).getEulerYPR(yaw1,pitch1,roll1);


            Emergency[0]=true;
            px[0] = tx1*1000;
            pz[0] = -ty1*1000;
            py[0] = tz1*1000;
            ppitch[0] = roll1*180/M_PI; //x
            proll[0] = pitch1*180/M_PI;//Y
            pyaw[0] = yaw1*180/M_PI; //Z

            ROS_INFO("px1=[%f], py1=[%f], pz1=[%f], proll1=[%f], ppitch1=[%f], pyaw1=[%f] \n",
                      px[0],py[0],pz[0],proll[0],ppitch[0],pyaw[0]);
 	    }

	catch (tf::TransformException ex){
            ROS_ERROR("%s",ex.what());
	    Emergency[0]=false;
        }

        try{
            listener.lookupTransform("/Vicon","/Rovio_two",ros::Time(0), transform);

      //Transformations
            double tx2 = transform.getOrigin().x();
            double ty2 = transform.getOrigin().y();
            double tz2 = transform.getOrigin().z();

            tf::Quaternion quat = transform.getRotation();
            double roll2, pitch2, yaw2;	   
//            tf::Matrix3x3(quat);
            tf::Matrix3x3(quat).getEulerYPR(yaw2,pitch2,roll2);

	    Emergency[1]=true;
	
            px[1] = tx2*1000;
            pz[1] = -ty2*1000;
            py[1] = tz2*1000;
            ppitch[1] = roll2*180/M_PI; //x
            proll[1] = pitch2*180/M_PI;//Y
            pyaw[1] = yaw2*180/M_PI; //Z

            ROS_INFO("px2=[%f], py2=[%f], pz2=[%f], proll2=[%f], ppitch2=[%f], pyaw2=[%f] \n",
                      px[1],py[1],pz[1],proll[1],ppitch[1],pyaw[1]);
 	    }

	catch (tf::TransformException ex){
            ROS_ERROR("%s",ex.what());
	    Emergency[1]=false;
        }

      try{
            listener.lookupTransform("/Vicon","/ARDrone2",ros::Time(0), transform);

      //Transformations
            double tx3 = transform.getOrigin().x();
            double ty3 = transform.getOrigin().y();
            double tz3 = transform.getOrigin().z();

            tf::Quaternion quat = transform.getRotation();
            double roll3, pitch3, yaw3;	   
//            tf::Matrix3x3(quat);
            tf::Matrix3x3(quat).getEulerYPR(yaw3,pitch3,roll3);

	    Emergency[2]=true;
	
            px[2] = tx3*1000;
            pz[2] = -ty3*1000;
            py[2] = tz3*1000;
            ppitch[2] = roll3*180/M_PI; //x
            proll[2] = pitch3*180/M_PI;//Y
            pyaw[2] = yaw3*180/M_PI; //Z

            ROS_INFO("px3=[%f], py3=[%f], pz3=[%f], proll3=[%f], ppitch3=[%f], pyaw3=[%f] \n",
                      px[2],py[2],pz[2],proll[2],ppitch[2],pyaw[2]);
 	    }

	catch (tf::TransformException ex){
            ROS_ERROR("%s ARDrone2",ex.what());
	    Emergency[2]=false;
        }

    try{
            listener.lookupTransform("/Vicon","/ARDrone3",ros::Time(0), transform);

      //Transformations
            double tx4 = transform.getOrigin().x();
            double ty4 = transform.getOrigin().y();
            double tz4 = transform.getOrigin().z();

            tf::Quaternion quat4 = transform.getRotation();
            double roll4, pitch4, yaw4;    
//            tf::Matrix3x3(quat);
            tf::Matrix3x3(quat4).getEulerYPR(yaw4,pitch4,roll4);


            Emergency[3]=true;
            px[3] = tx4*1000;
            pz[3] = -ty4*1000;
            py[3] = tz4*1000;
            ppitch[3] = roll4*180/M_PI; //x
            proll[3] = pitch4*180/M_PI;//Y
            pyaw[3] = yaw4*180/M_PI; //Z

            ROS_INFO("px4=[%f], py4=[%f], pz4=[%f], proll4=[%f], ppitch4=[%f], pyaw4=[%f] \n",
                      px[3],py[3],pz[3],proll[3],ppitch[3],pyaw[3]);
        }

    catch (tf::TransformException ex){
            ROS_ERROR("%s",ex.what());
        Emergency[3]=false;
        }

            vicon::position msg;

            msg.x=px[0];
            msg.y=py[0];
            msg.z=pz[0];

            msg.roll=proll[0];
            msg.pitch=ppitch[0];
            msg.yaw=pyaw[0];
	    msg.Emer=Emergency[0];

            msg.mx=px[1];
            msg.my=py[1];
            msg.mz=pz[1];

            msg.mroll=proll[1];
            msg.mpitch=ppitch[1];
            msg.myaw=pyaw[1];
	    msg.mEmer=Emergency[1];

            msg.ux=px[2];
            msg.uy=py[2];
            msg.uz=pz[2];

            msg.uroll=proll[2];
            msg.upitch=ppitch[2];
            msg.uyaw=pyaw[2];
        msg.uEmer=Emergency[2];
            pub.publish(msg);

            msg.ux=px[2];
            msg.uy=py[2];
            msg.uz=pz[2];

            msg.uroll=proll[2];
            msg.upitch=ppitch[2];
            msg.uyaw=pyaw[2];
        msg.uEmer=Emergency[2];
            pub.publish(msg);

            msg.UAV_3_x=px[3];
            msg.UAV_3_y=py[3];
            msg.UAV_3_z=pz[3];

            msg.UAV_3_roll=proll[3];
            msg.UAV_3_pitch=ppitch[3];
            msg.UAV_3_yaw=pyaw[3];
        msg.UAV_3_Emer=Emergency[3];
            pub.publish(msg);

            ros::spinOnce();        
        rate.sleep();
    }
    return 0;
}
