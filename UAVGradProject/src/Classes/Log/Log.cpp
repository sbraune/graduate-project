#include "Log.h"

Log::Log(std::string filename){
  //file.open ("./logs/test.csv");
  file.open (filename.c_str());
  file << "time(ms)\tx\ty\tz\troll\tpitch\tyaw\tvelocity\n";
  startTime = 0;
  curTime = 0;

}

Log::~Log(){
  file.close();
}

void Log::write(Vec4<float> pos, Vec3<float> orient){
  boost::thread worker(&Log::threadWrite,this,pos,orient);
}


void Log::threadWrite(Vec4<float> pos, Vec3<float> orient){

  pos.print();
  
  write_mux.lock();
  gettimeofday(&time, NULL);
  curTime = (time.tv_sec * 100) + (time.tv_usec / 1000);

  std::cout << (curTime - startTime)/100.0 << std::endl;

  file << (curTime - startTime)/100.0  << '\t'
         << pos.getX() << '\t'  
         << pos.getY()  << '\t' 
         << pos.getZ()  << '\t' 
         << orient.getX() << '\t' 
         << orient.getY()  << '\t'  
         << orient.getZ()  << '\t'  
         << 0.0 << '\t'  << '\n';

  write_mux.unlock();

}

void Log::setStartTime(){
    timeval time;

  gettimeofday(&time, NULL);
  startTime = (time.tv_sec * 100) + (time.tv_usec / 1000);
}