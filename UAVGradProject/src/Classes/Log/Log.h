/**
 @class     Log
 @brief     Basic Log Class for logging flight
 @details   This class allows the the ability to allow simple logging
            for unmaned flight.
 @author    Shawn Braune
 @version   1.0
 @date      2015

 @warning   Velocity is not imlemented
 */
#ifndef LOG_H
#define LOG_H

#include <fstream>
#include <ctime>
#include <cstdlib>
#include <sys/time.h>
#include "../Vec4/Vec4.h"
#include <iostream>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>


 
class Log { 

  public:

    /** @brief Default Constructor

    This is the Default Constructor for the Log class. 

    @param string Is the path for the log file


    @author Shawn Braune
    @date Feb 2015
    */
    Log(std::string filename);


    /** @brief  De-Constructor

    This is the Default Constructor for the Log class. 

    @author Shawn Braune
    @date Feb 2015
    */
   
    ~Log();

    /** @brief Writing to Log

    This allows the main way to write to a log file. This function calls
    threadWrite which runs the write function in a seperate thread

    @param Vec4<TYPE> Position of Aircraft
    @param Vec3<TYPE> Inertial frame of Aircraft

    @see threadWrite

    @author Shawn Braune
    @date Feb 2015
    */   
    void write(Vec4<float>,Vec3<float>);

    /** @brief Sets the Start Time of the Log Class

    This sets the start time of the Log class which allows for
    a somewhate accurate time of the log class

    @author Shawn Braune
    @date Feb 2015
    */ 
    void setStartTime();




private:
  /**The output file*/
  std::ofstream file;
  /**The start time*/
  unsigned long long startTime;
  /**The current time*/
  unsigned long long curTime;
  /**time struct*/
  timeval time;
  /**Mutex to prevent race condition when logging*/
  boost::mutex write_mux;
  
  /** @brief Writing to Log in a thread

  This function is called from the write function is done in
  a seperate thread

  @param Vec4<TYPE> Position of Aircraft
  @param Vec3<TYPE> Inertial frame of Aircraft

  @see write

  @author Shawn Braune
  @date Feb 2015
  */     
  void threadWrite(Vec4<float>,Vec3<float>);
};

#endif