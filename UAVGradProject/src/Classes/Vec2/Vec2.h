/**
 @class     Vec3
 @brief     Vec3 Templated Class for UAV Systems
 @details   This class is a templated Vec3 class tht is used in conjuction with 
            UAV Systems
 @author    Shawn Braune
 @version   1.0
 @date      2014
 @pre       First initialize the system.

 @warning   Improper use can crash your application
 */
#ifndef VEC2_H
#define VEC2_H

#include <iostream>
#include <cassert>


template <class TYPE> 
class Vec2 {
  
  public:

    /** @brief Default Constructor

    This is the Default Constructor for the Vec2 class. It initializes and sets
    all values to zero.

    @author Shawn Braune
    @date October 2014
    */
    Vec2();

    /** @brief Copy Constructor

    This is the Copy Consstructor for passing Vec2 by value or to make a copy of
    a Vec2. 

    @param Vec2<TYPE> Vector 2 to be copied and *Must Be of Vec2 Type*

    @author Shawn Braune
    @date October 2014
    */
    Vec2(const Vec2<TYPE>&);

    /** @brief Constructor

    This Constructor can be used to intialize a Vec2<TYPE> with TYPE values. 

    @param TYPE Value for X and *Must Be of Vec3 Type*
    @param TYPE Value for Y and *Must Be of Vec3 Type*

    @author Shawn Braune
    @date October 2014
    */
    Vec2(const TYPE &xVal, const TYPE &yVal);

    /** @brief Destructor

    Dealocates memory and sets value to NULL

    @author Shawn Braune
    @date October 2014
    */
    ~Vec2();

    //Getters and Setters
    /** @brief Set X and Y Values

    This sets the X and Y cordinates respectivly. 

    @param TYPE Value for X and *Must Be of Vec2 Type*
    @param TYPE Value for Y and *Must Be of Vec2 Type*

    @see setX()
    @see setY()

    @b Example
    @code
     //Example using float Type

     Vec2<float> vec;

     TYPE xVal = 3.5,
          yVal = 2.2,

     vec.set(xval,yval);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    virtual void set(const TYPE &xVal, const TYPE &yVal);

    /** @brief Set X Value

    This sets the X value respectivly. 

    @param TYPE Value for X and *Must Be of Vec2 Type*

    @see set()
    @see setY()

    @b Example
    @code
     //Example using float Type

     Vec2<float> vec;

     TYPE xVal = 3.5;

     vec.setX(xval);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void setX(const TYPE &val);

    /** @brief Set Y Value

    This sets the Y value respectivly. 

    @param TYPE Value for Y and *Must Be of Vec2 Type*

    @see set()
    @see setX()

    @b Example
    @code
     //Example using float Type

     Vec2<float> vec;

     TYPE yVal = 2.2;

     vec.setY(yval);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void setY(const TYPE &val);


    /** @brief Get X

    This Gets the X value and returns the value of TYPE Vec2<TYPE>. 

    @returns TYPE Value for X

    @see getY()

    @b Example
    @code
     //Example using float Type

     Vec2<float> vec;

     float xVal;

     xVal = vec.getX();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    TYPE getX() const;

    /** @brief Get Y

    This Gets the Y value and returns the value of TYPE Vec2<TYPE>. 

    @returns TYPE Value for Y

    @see getX()

    @b Example
    @code
     //Example using float Type

     Vec2<float> vec;

     float yVal;

     yVal = vec.getY();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    TYPE getY() const;

    //Helper
    /** @brief Zero

    A helper function that sets the vector to zero

    @b Example
    @code
     //Example using float Type

     Vec2<float> vec;

     vec.zero();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    virtual void zero();

    /** @brief Print

    A helper function that prints the vector to screen

    @b Example
    @code
     //Example using float Type

     Vec2<float> vec;

     vec.print();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    virtual void print();

    /** @brief Equals

    This is an overloaded equals operator that takes a Vec3<TYPE>
    on the left side and copies it to the Vec3<TYPE> on the left side.

    @returns Vec3<TYPE> Vector

    @b Example
    @code
     //Example using float Type

     Vec2<float> vec1,vec2;

     vec1 = vec2;
    @endcode

    @author Shawn Braune
    @date MArch 2015
    */    
    Vec2<TYPE> operator=(const Vec2<TYPE> &vec) const;

    /** @brief []

    This is an overloaded [] operator. It allows access to the values of 
    Vec3.

    @returns TYPE

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec1[0] = vec2[1];
    @endcode

    @author Shawn Braune
    @date March 2015
    */ 
    TYPE& operator[] (const int) ;

  protected:

    TYPE *x, /**< TYPE pointer X */ 

         *y; /**< TYPE pointer Y */ 

};

#endif