#include "UAV.h"

UAV::UAV(UAV_Params params){

  params.print();

  
  frame = new Frame(0, params.name.c_str());
  control = new Control(params.con_File.c_str(), 1/(float)params.looprate); 
  log = new Log("./logs/" + params.logfile);
  state = LANDED;
  id = params.id;
  looprate = params.looprate;

  pubLand = n.advertise<std_msgs::Empty>(params.land.c_str(), 1000);
  pubTakeOff = n.advertise<std_msgs::Empty>(params.takeoff.c_str(), 1000);
  pubReset = n.advertise<std_msgs::Empty>(params.reset.c_str(), 1000);

  pubControl = n.advertise<geometry_msgs::Twist>(params.control.c_str(), 1000);

  navdata = n.subscribe(params.navData.c_str(), 1000, &Frame::updateBatteryStatus, frame); // read battery percent
  vicon = n.subscribe(params.vicon.c_str(), 1000, &Frame::updatePosition, frame); // read Vicon*/
  waypoints = new Waypoint(params.waypoints.c_str());

  patterns.push_back(new Waypoint(params.pattern1.c_str()));
  patterns.push_back(new Waypoint(params.pattern2.c_str()));
  patterns.push_back(new Waypoint(params.pattern3.c_str()));
  patterns.push_back(new Waypoint(params.pattern4.c_str()));

  waypoints->print();
  
}


UAV::~UAV(){
  delete frame;
  delete control;
  delete waypoints;
  delete log;
}

void UAV::hover(){
  if(state != HOVER){
    hoverLoc = Vec4<float>(frame->getXCord(), frame->getYCord(), 800.0, frame->getYaw());
    state = HOVER;
  }
  stable(hoverLoc);
  logAction();
}

void UAV::hover(float elevation){
  if(state != HOVER){
    hoverLoc = Vec4<float>(frame->getXCord(), frame->getYCord(), elevation, frame->getYaw());
    state = HOVER;
  }
  //control->Hover();
  //control->publishHoverControl(&pubControl);
  stable(hoverLoc);
  logAction();
}

void UAV::land(){
  if(state != LANDED){
    state = LANDED;
    publishMessage(LAND);
  }
}

void UAV::takeoff(){
  if(state == LANDED){
    log->setStartTime();
    publishMessage(TAKEOFF);
  }
  //logAction();
}

void UAV::reset(){
  publishMessage(RESET);
}

void UAV::logAction(){

  log->write(frame->getFlightPos(), frame->getRotation());
}

int UAV::getState() const {
  return state;
}

std_msgs::Int16MultiArray UAV::getUAVData(){
  short values[10];
  std_msgs::Int16MultiArray array;
  array.data.clear();

  values[0] = id;
  values[1] = frame->getXCord() ;
  values[2] = frame->getYCord();
  values[3] = frame->getZCord();
  values[4] = frame->getPitch();
  values[5] = frame->getRoll();
  values[6] = frame->getYaw();
  values[7] = frame->getFuel();
  values[8] = state;

  values[9] = values[0] + fabs(values[1]) + fabs(values[2]) + fabs(values[3]) +
              fabs(values[4]) + fabs(values[5]) + fabs(values[6]) + values[7] + values[8];

  for (int i = 0; i < 10; i++)
    {
      //assign array a random number between 0 and 255.
      array.data.push_back(values[i]);
      //std::cout << values[i] << "\n";
    }

    return array;
}

void UAV::gotoLoc(int index){
  Vec4<float> loc = waypoints->getWaypoint(index);
  stable(loc);
}

void UAV::pattern(int pattern){

  if(pattern <= 0 || pattern > 4)
    return;

  Vec4<float> curWaypoint = patterns[pattern-1]->getCurWaypoint();
  Vec4<float> delta;

  static int timer = 0;
  static int elapsed  = 0;
  static int patternState = 1;

  switch(patternState){
    case 0 :  //Hover
              //if(timer == looprate/2 )
                ROS_INFO("Hover: %i\n", elapsed); 

              //hover(control,frame,publisher);
              stable(curWaypoint);

              if(timer == 600/*freq*/){
                timer = 0;
                elapsed++;

                if(elapsed == 3){
                  patternState = 1;
                  patterns[pattern-1]->increment();
                  elapsed = 0;
                }
              }
              else{
                timer++;
              }
              break;
    case 1 :  //Fly
             // if(timer == looprate/2 )
              ROS_INFO("Fly:\n"); 
              stable(curWaypoint);

              delta = curWaypoint - frame->getFlightPos();
              delta.print();

              if(delta[0] < 100.0 && delta[1] < 100.0 && delta[2] < 100.0 && delta[3] < 100.0)
              {
                //ROS_INFO("State Changing to Hover:\n"); 
                patternState = 0;
                state = FLYING;
              }

             break;
    
  }
}

void UAV::print(){

  boost::thread worker(&UAV::threadPrint,this);
}

void UAV::threadPrint(){

  print_mux.lock();
  frame->printInfo();
  waypoints->getCurWaypoint().print();
  print_mux.unlock();

}

void UAV::stable(Vec4<float> newPos){
  boost::thread worker(&UAV::threadStable,this, newPos);

}

void UAV::threadStable(Vec4<float> &newPos){
  control_mux.lock();
  Vec4<float> myPos = frame->getFlightPos();
  control->controlV2(newPos, myPos,true);
  control->publishMessageControl(&pubControl);
  logAction();
  control_mux.unlock();

}

void UAV::publishMessage(int stage)
{
  std_msgs::Empty msg;  

  switch(stage){
    case LAND    : pubLand.publish(msg);
                   break;

    case TAKEOFF : pubTakeOff.publish(msg);
                   break;

    case RESET   : pubReset.publish(msg);
  }
}
