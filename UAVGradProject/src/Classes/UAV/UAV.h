/**
 @class     UAV
 @brief     UAV class 
 @details   This is the main class for controlling and getting UAV data.
            All other classes are communicated to through this class
 @author    Shawn Braune
 @version   1.0
 @date      Feb 2015
 @pre       First initialize the system and ROS

 @warning   Improper use can crash your application
 */
#ifndef UAV_H
#define UAV_H

#include <iostream>
#include <std_msgs/Empty.h>
#include "../Frame/Frame.h"
#include "../Control/Control.h"
#include "../Waypoint/waypoint.h"
#include "../Log/Log.h"
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>
#include <sstream>
#include "std_msgs/String.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Int16MultiArray.h"

/** Landed State Constant*/
const int LANDED = 0;
/**Flying State Constant*/
const int FLYING = 1;
/**Hover State Constant*/
const int HOVER = 2;

/** UAV Parameters An Struct type.

@brief This is the structrue to use to pass the UAV_Parameters
@author Shawn Braune
@date MArch 2015
*/
struct UAV_Params{

  std::string name,/**Name of the UAV*/
         con_File, /**The Path to the Control File*/
         land,     /**The Land ROS Topic*/
         takeoff,  /**Takeoff ROS Topic*/
         reset,    /**Reset ROS Topic*/
         control,  /**Control ROS Topic*/
         navData,  /**NavData ROS Topic*/
         vicon,    /**ARDroneGPS Data ROS Topic*/
         waypoints,/**Waypoint File Path*/
         pattern1, /**Pattern File Path*/
         pattern2, /**Pattern File Path*/
         pattern3, /**Pattern File Path*/
         pattern4, /**Pattern File Path*/
         logfile;  /**Log File Path*/

         int looprate, /**Looprate of ROS Loop*/
             id;       /**UAV ID*/
  
  /** @brief Set values of Struct

  This allows you to set the values of the UAV_Params struct

  @param std::string Name of the UAV
  @param std::string The Path to the Control File
  @param std::string The Land ROS Topic
  @param std::string Takeoff ROS Topic
  @param std::string Reset ROS Topic
  @param std::string Control ROS Topic
  @param std::string NavData ROS Topic
  @param std::string ARDroneGPS Data ROS Topic
  @param std::string Waypoint File Path
  @param std::string Pattern File Path
  @param std::string Pattern File Path
  @param std::string Pattern File Path
  @param std::string Pattern File Path
  @param std::string Log File Path
  @param int Looprate of ROS Loop
  @param int UAV ID

  @author Shawn Braune
  @date March 2014
  */    
  void set(std::string _name, std::string _conFile, std::string _land,
           std::string _takeoff, std::string _reset, std::string _control,
           std::string _navData,std::string _vicon, std::string _waypoints, 
           std::string _pattern1, std::string _pattern2, std::string _pattern3, std::string _pattern4,  
           std::string _logfile, int _looprate, int _id){

    name = _name;
    con_File = _conFile;
    land = _land;
    takeoff = _takeoff;
    reset = _reset;
    control = _control;
    navData = _navData;
    vicon = _vicon;
    waypoints = _waypoints;
    pattern1 = _pattern1;
    pattern2 = _pattern2;
    pattern3 = _pattern3;
    pattern4 = _pattern4;
    logfile = _logfile;
    looprate = _looprate;
    id = _id;
  }

  /** @brief Print

  A helper function that prints the contents of the UAV_Params Struct
  @author Shawn Braune
  @date March 2015
  */
  void print(){
    std::cout << "UAV Name     : " << name << "\n"
              << "Control File : " << con_File << "\n"
              << "Land         : " << land << "\n"
              << "Takeoff      : " << takeoff << "\n"
              << "Reset        : " << reset << "\n"
              << "Control      : " << control << "\n"
              << "Nav Data     : " << navData << "\n"
              << "Vicon        : " << vicon << "\n"
              << "Looprate     : " << looprate << "\n";
  }
};

 
class UAV { 

  public:

    /** @brief Default Constructor

    This is the Default Constructor for the UAV class. 

    @author Shawn Braune
    @date Feb 2015
    */
    UAV(UAV_Params);


    /** @brief  De-Constructor

    This is the Default Constructor for the UAV class. It initializes and sets
    all values to zero.

    @author Shawn Braune
    @date Feb 2015
    */
    ~UAV();

    /** @brief Hover UAV

    This allows you to initiate hover over an elevation of 800

    @author Shawn Braune
    @date April 2015
    */
    void hover();

    /** @brief Hover UAV

    This allows you to initiate hover over an elevation chosen by
    the programer

    @param float Elevation 

    @author Shawn Braune
    @date April 2015
    */
    void hover(float);

    /** @brief  Land UAV

    This is allows you initiate landing

    @author Shawn Braune
    @date Feb 2015
    */
    void land();

    /** @brief  Takeoff UAV

    This is allows you initiate Takeoff

    @author Shawn Braune
    @date Feb 2015
    */
    void takeoff();

    /** @brief  Reset UAV

    This is allows you initiate reset of the UAV

    @author Shawn Braune
    @date April 2015
    */
    void reset();

    /** @brief  Stable UAV

    This is allows you initiate stable flight of the UAV by passing
    the target vector and passing it to threadStable to allow it to
    run in a different thread

    @param Vec4<float> Target Position

    @see threadStable()

    @author Shawn Braune
    @date MArch 2015
    */
    void stable(Vec4<float>);

    /** @brief  Goto a specific location

    This is allows you to go to a waypoint 
    waypoint 

    @param int waypoint index

    @author Shawn Braune
    @date MArch 2015
    */
    void gotoLoc(int);

    /** @brief  UAV Pattern

    This is allows you initiate a waypoint pattern

    @param int pattern number

    @author Shawn Braune
    @date MArch 2015
    */
    void pattern(int pattern);

    /** @brief  Get the UAV Data

    This allows you to get the waypoint data and send it 
    to the Glass Node

    @param std_msgs::Int16MultiArray UAV Data

    @author Shawn Braune
    @date MArch 2015
    */
    std_msgs::Int16MultiArray getUAVData();

    /** @brief  Print UAV Data

    This is allows you to print the UAV data to to screen and calls
    the threadPrint function to run it in a seperate thread

    @see threadPrint

    @author Shawn Braune
    @date MArch 2015
    */
    void print();


    /** @brief  Log UAV Data

    This allows you to log the data of the UAV.
    
    @author Shawn Braune
    @date MArch 2015
    */
    void logAction();

    /** @brief  Get state of the UAV

    This allows you to get the State of the UAV

    @author Shawn Braune
    @date MArch 2015
    */
    int getState() const;

  private:
    /**Land Publish Constant*/
    static const int LAND = 0;
    /**Takeoff Publish Constant*/
    static const int TAKEOFF = 1;
    /**Reset Publish Constant*/
    static const int RESET = 2;

    /**Frame of the UAV*/
    Frame *frame;
    /**Control of the UAV*/
    Control *control; 
    /**Waypoints of the UAV*/
    Waypoint *waypoints;
    /**Patterns of the UAV*/
    std::vector<Waypoint*> patterns;
    /**Log of the UAV*/
    Log *log;

    /**Hover Location of the UAV*/
    Vec4<float> hoverLoc;
    int state,    /**State of the UAV*/
        id,       /**ID of the UAV*/
        looprate; /**Looprate of ROS*/

    /**UAV Land Publisher*/
    ros::Publisher pubLand;
    /**UAV Takeoff Publisher*/  
    ros::Publisher pubTakeOff;
    /**UAV Reset Publisher*/
    ros::Publisher pubReset;
    /**UAV Control Publisher*/
    ros::Publisher pubControl;
    /**UAV Navdata Subscriber*/
    ros::Subscriber navdata;
    /**UAV ARDroneGPS Vicon Data Subscriber*/
    ros::Subscriber vicon;
    /**UAV Node Handle*/
    ros::NodeHandle n;
    /**UAV Print Mutex*/
    boost::mutex print_mux;
    /**UAV Control Mutex*/
    boost::mutex control_mux;

    /** @brief  Prints UAV Data in a thread

    This is allows you to print the UAV Data 
    in a seperate thread which is protected by 
    a mutex

    @see print()

    @author Shawn Braune
    @date MArch 2015
    */
    void threadPrint();

    /** @brief  UAV Stable Flight in a thread

    This is allows you to run the stable UAV flight
    in a thread protected by the mutex.

    @see stable

    @author Shawn Braune
    @date MArch 2015
    */
    void threadStable(Vec4<float>&);

    /** @brief  Publishes  Takeoff, Land and Reset

    This is allows you to publish a message for takeoff,
    land and reset

    @see takeoff
    @see land
    @see reset

    @author Shawn Braune
    @date MArch 2015
    */
    void publishMessage(int stage);
};
#endif