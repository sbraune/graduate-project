#include "Control.h"

Control::Control(){

  errorX = new Vec2<float>;
  errorY =  new Vec2<float>;
  errorZ =  new Vec2<float>;
  errorYaw = new Vec2<float>;

  inertialFrame = new Vec3<float>();
  fixedFrame = new Vec4<float>();

  dirvEurlLevIntErX  = new Vec3<float>();
  dirvEurlLevIntErY  = new Vec3<float>();
  dirvEurlLevIntErZ = new Vec3<float>();
  dirvEurlLevIntErYaw =  new Vec3<float>();

  //get 
  CP_X = new Vec3<float>(0.25,0.35,0.005);
  CP_Y = new Vec3<float>(0.25,0.35,0.005);
  CP_Z = new Vec3<float>(0.5,0.65,0.05);
  CP_Yaw = new Vec3<float>(0.9,0.08,0.05);
  Sat = new Vec4<float>(0.3,0.3,0.3,0.8);

  rotz = new Mat3<float>; 

}

Control::Control(std::string fileName, float _H){

  float params[16];

  H = new float;
  *H = _H;

  std::cout << *H << "\n";

  errorX = new Vec2<float>;
  errorY =  new Vec2<float>;
  errorZ =  new Vec2<float>;
  errorYaw = new Vec2<float>;

  inertialFrame = new Vec3<float>();
  fixedFrame = new Vec4<float>();

  dirvEurlLevIntErX  = new Vec3<float>();
  dirvEurlLevIntErY  = new Vec3<float>();
  dirvEurlLevIntErZ = new Vec3<float>();
  dirvEurlLevIntErYaw =  new Vec3<float>();

  //get params from file

  getParamsFromFile(params, fileName);

  CP_X = new Vec3<float>(params[0],params[1],params[2]);
  CP_Y = new Vec3<float>(params[3],params[4],params[5]);
  CP_Z = new Vec3<float>(params[6],params[7],params[8]);
  CP_Yaw = new Vec3<float>(params[9],params[10],params[11]);
  Sat = new Vec4<float>(params[12],params[13],params[14],params[15]);

  CP_X->print();
  CP_Y->print();
  CP_Z->print();
  CP_Yaw->print();
  Sat->print();

  rotz = new Mat3<float>; 
}

Control::~Control(){
  delete errorX;
  delete errorY;
  delete errorZ;
  delete errorYaw;

  delete inertialFrame;
  delete fixedFrame;

  delete dirvEurlLevIntErX;
  delete dirvEurlLevIntErY;
  delete dirvEurlLevIntErZ;
  delete dirvEurlLevIntErYaw;

  delete CP_X;
  delete CP_Y;
  delete CP_Z;
  delete CP_Yaw;
  delete Sat;
  delete H;
}

bool Control::getParamsFromFile(float (&params)[16], std::string fileName){

  std::string line;
  std::ifstream myfile( fileName.c_str());

  //float params[12];
  int count = 0;
  
  if (myfile){
    while (getline( myfile, line )){

      if(line[0] != '#'){
        std::istringstream ss( line );

        while (ss){

          std::string s;
          if (!getline( ss, s, ',' )) break;
         
          params[count] = atof(s.c_str());
          std::cout << count << " " << params[count] << "\n";
          count++;
        }
      }
    }
    
    myfile.close();
  }
  else std::cout << "Something with the file went wrong\n";


  return true;
}

void Control::hover(){
  inertialFrame->setX(0.0); 
  inertialFrame->setY(0.0); 
  inertialFrame->setZ(0.0);
  fixedFrame->setA(0.0);
}

float Control::eulerIntegration(float valant,float varIntE){
  return valant + (*H * varIntE);
} 

float Control::eulerDerivative(float varDevEAct, float varDevEAnt){
  return (varDevEAct-varDevEAnt) / *H;
} 

float Control::levantDerivative(float fntoDev,int pos){
  int sa;

  levant[pos].setX(eulerIntegration(levant[pos].getX(),levant[pos].getY()));
  levant[pos].setZ(eulerIntegration(levant[pos].getZ(),levant[pos].getA()));  

  sa = levant[pos].getZ()-fntoDev;
    
  levant[pos].setA(-95*sqrt(fabs(sa))*(sa/(fabs(sa)+0.01))+levant[pos].getX());
  levant[pos].setY(-2000*(sa)/(fabs(sa)+0.01));      
  return levant[pos].getX();
}

void Control::control(Vec4<float> &targetPos, Vec4<float> &currentPos, bool OC)
{

  if(OC==true) 
  {
    //Find the Error
    errorX->setX(targetPos.getX() - currentPos.getX());
    dirvEurlLevIntErX->setX(eulerDerivative(errorX->getX(),errorX->getY()));
    dirvEurlLevIntErX->setY(levantDerivative(errorX->getX(),0));
    dirvEurlLevIntErX->setZ(eulerIntegration(dirvEurlLevIntErX->getZ(), errorX->getX()));


    errorY->setX(targetPos.getY() - currentPos.getY());
    dirvEurlLevIntErY->setX(eulerDerivative(errorY->getX(),errorY->getY()));
    dirvEurlLevIntErY->setY(levantDerivative(errorY->getX(),1));
    dirvEurlLevIntErY->setZ(eulerIntegration(dirvEurlLevIntErY->getZ(), errorY->getX()));


    errorZ->setX(targetPos.getZ() - currentPos.getZ());
    dirvEurlLevIntErZ->setX(eulerDerivative(errorZ->getX(),errorZ->getY()));
    dirvEurlLevIntErZ->setY(levantDerivative(errorZ->getX(),2));
    dirvEurlLevIntErZ->setZ(eulerIntegration(dirvEurlLevIntErZ->getZ(), errorZ->getX()));


    errorYaw->setX(targetPos.getA() - currentPos.getA());
    if(errorYaw->getX() > 180)
      errorYaw->setX(errorYaw->getX() - 360.0);
    else if (errorYaw->getX() < -180)
      errorYaw->setX(errorYaw->getX() + 360.0);

    dirvEurlLevIntErYaw->setX(eulerDerivative(errorYaw->getX(),errorYaw->getY()));
    dirvEurlLevIntErYaw->setY(levantDerivative(errorYaw->getX(),3));
    dirvEurlLevIntErYaw->setZ(eulerIntegration(dirvEurlLevIntErYaw->getZ(), errorYaw->getX()));


    //Get new fixed frame cords
    fixedFrame->setX(-((CP_Y->getY()* dirvEurlLevIntErY->getX()) + 
                    (CP_Y->getX() * errorY->getX()) + 
                    (CP_Y->getZ() * dirvEurlLevIntErY->getZ())) / 1000);

    //Saturate 
    if(fixedFrame->getX() > Sat->getX())
      fixedFrame->setX(Sat->getX());
    else if(fixedFrame->getX() < -Sat->getX())
      fixedFrame->setX(-Sat->getX());

    fixedFrame->setY(-((CP_X->getY()* dirvEurlLevIntErX->getX()) + 
                    (CP_X->getX() * errorX->getX()) + 
                    (CP_X->getZ() * dirvEurlLevIntErX->getZ())) / 1000);

    //Saturate 
    if(fixedFrame->getY() > Sat->getY())
      fixedFrame->setY(Sat->getY());
    else if(fixedFrame->getY() < -Sat->getY())
      fixedFrame->setY(-Sat->getY());

    fixedFrame->setZ(((CP_Z->getY()* dirvEurlLevIntErZ->getY()) + 
                    ((CP_Z->getX() * errorZ->getX()) + 
                    (CP_Z->getZ() * dirvEurlLevIntErZ->getZ()))-9.81) / 100);

    //Saturate 
    if(fixedFrame->getZ() > Sat->getZ())
      fixedFrame->setZ(Sat->getZ());
    else if(fixedFrame->getZ() < -Sat->getZ())
      fixedFrame->setZ(-Sat->getZ());

    fixedFrame->setA(((CP_Yaw->getY()* dirvEurlLevIntErYaw->getY()) + 
                    (CP_Yaw->getX() * errorYaw->getX()) + 
                    (CP_Yaw->getZ() * dirvEurlLevIntErYaw->getZ()))/1000);

    //Saturate 
    if(fixedFrame->getA() > Sat->getA())
      fixedFrame->setA(Sat->getA());
    else if(fixedFrame->getA() < -Sat->getA())
      fixedFrame->setA(-Sat->getA());

    //Find rotation of inertial frame
    rotz->createRotation(currentPos.getA());
    *inertialFrame = *rotz * *fixedFrame;

    //Saturate Inertial frame
    if(inertialFrame->getX() > Sat->getX())
      inertialFrame->setX(Sat->getX());
    else if(inertialFrame->getX() < -Sat->getX())
      inertialFrame->setX(-Sat->getX());

    if(inertialFrame->getY() > Sat->getY())
      inertialFrame->setY(Sat->getY());
    else if(inertialFrame->getY() < -Sat->getY())
      inertialFrame->setY(-Sat->getY());

    if(inertialFrame->getZ() > Sat->getZ())
      inertialFrame->setZ(Sat->getZ());
    else if(inertialFrame->getZ() < -Sat->getZ())
      inertialFrame->setZ(-Sat->getZ());

    errorX->setY(targetPos.getX() - currentPos.getX());
    errorY->setY(targetPos.getY() - currentPos.getY());
    errorZ->setY(targetPos.getZ() - currentPos.getZ());
    errorYaw->setY(targetPos.getA() - currentPos.getA());

    if(errorYaw->getY() > 180)
      errorYaw->setY(errorYaw->getY() - 360.0);
    else if (errorYaw->getY() < -180)
      errorYaw->setY(errorYaw->getY() + 360.0);
  }
  if(OC==false)
  {
    inertialFrame->setX(0.00);
    inertialFrame->setY(0.00);
    inertialFrame->setZ(0.00);
    fixedFrame->setA(0.00);
  }
}

void Control::controlV2(Vec4<float> &targetPos, Vec4<float> &currentPos, bool OC)
{

  if(OC==true) 
  {
     (*errorX)[0] = targetPos[0] - currentPos[0];
     (*dirvEurlLevIntErX)[0] = eulerDerivative((*errorX)[0], (*errorX)[1]);
     (*dirvEurlLevIntErX)[1] = levantDerivative((*errorX)[0],0);
     (*dirvEurlLevIntErX)[2] = eulerIntegration((*dirvEurlLevIntErX)[2],(*errorX)[0]);


    (*errorY)[0] = targetPos[1] - currentPos[1];
    (*dirvEurlLevIntErY)[0] = eulerDerivative((*errorY)[0], (*errorY)[1]);
    (*dirvEurlLevIntErY)[1] = levantDerivative((*errorY)[0],1);
    (*dirvEurlLevIntErY)[2] = eulerIntegration((*dirvEurlLevIntErY)[2], (*errorY)[0]);


    (*errorZ)[0] = targetPos[2] - currentPos[2];
    (*dirvEurlLevIntErZ)[0] = eulerDerivative((*errorZ)[0], (*errorZ)[1]);
    (*dirvEurlLevIntErZ)[1] = levantDerivative((*errorZ)[0],2);
    (*dirvEurlLevIntErZ)[2] = eulerIntegration((*dirvEurlLevIntErZ)[2], (*errorZ)[0]);


    (*errorYaw)[0] = targetPos[3] - currentPos[3];

    if((*errorYaw)[0] > 180)
      (*errorYaw)[0] = (*errorYaw)[0] - 360.0;
    else if ((*errorYaw)[0] < -180)
       (*errorYaw)[0] = (*errorYaw)[0] + 360.0;

    (*dirvEurlLevIntErYaw)[0] = eulerIntegration((*errorYaw)[0], (*errorYaw)[1]);
    (*dirvEurlLevIntErYaw)[1] = levantDerivative((*errorYaw)[0],3);
    (*dirvEurlLevIntErYaw)[2] = eulerIntegration((*dirvEurlLevIntErYaw)[2], (*errorYaw)[0]);


    //Get new fixed frame cords
    //X
    (*fixedFrame)[0] = -(((*CP_Y)[1] * (*dirvEurlLevIntErY)[0]) + 
                    ((*CP_Y)[0] * (*errorY)[0]) + 
                    ((*CP_Y)[2] * (*dirvEurlLevIntErY)[2])) / 1000.0;

    //Saturate 
    if((*fixedFrame)[0] > (*Sat)[0])
       (*fixedFrame)[0] = (*Sat)[0];
    else if( (*fixedFrame)[0] < -(*Sat)[0])
       (*fixedFrame)[0] = -(*Sat)[0];

    //Y
    (*fixedFrame)[1] = -(((*CP_X)[1] * (*dirvEurlLevIntErX)[0]) + 
                    ((*CP_X)[0] * (*errorX)[0]) + 
                    ((*CP_X)[2] * (*dirvEurlLevIntErX)[2])) / 1000.0;

    //Saturate 
    if((*fixedFrame)[1] > (*Sat)[1])
       (*fixedFrame)[1] = (*Sat)[1];
    else if( (*fixedFrame)[1] < -(*Sat)[1])
       (*fixedFrame)[1] = -(*Sat)[1];

    //Z
    (*fixedFrame)[2] = -(((*CP_Z)[1] * (*dirvEurlLevIntErZ)[1]) + 
                    ((*CP_Z)[0] * (*errorZ)[0]) + 
                    ((*CP_Z)[2] * (*dirvEurlLevIntErZ)[2])) / 1000.0;

    //Saturate 
    if( (*fixedFrame)[2] > (*Sat)[2])
       (*fixedFrame)[2] = (*Sat)[2];
    else if( (*fixedFrame)[2] < -(*Sat)[2])
       (*fixedFrame)[2] = -(*Sat)[2];

    //Yaw
    (*fixedFrame)[3] = (((*CP_Yaw)[1] * (*dirvEurlLevIntErYaw)[1]) + 
                   ((*CP_Yaw)[0] * (*errorYaw)[0]) + 
                    ((*CP_Yaw)[2] * (*dirvEurlLevIntErYaw)[2]))/1000.0;

    //Saturate 
    if( (*fixedFrame)[3] > (*Sat)[3])
       (*fixedFrame)[3] = (*Sat)[3];
    else if( (*fixedFrame)[3] < -(*Sat)[3])
       (*fixedFrame)[3] = -(*Sat)[3];  

    //Find rotation of inertial frame
    rotz->createRotation(currentPos[3]);
    *inertialFrame = *rotz * *fixedFrame;

    //Saturate Inertial frame
    if((*inertialFrame)[0] > (*Sat)[0])
      (*inertialFrame)[0] = (*Sat)[0];
    else if((*inertialFrame)[0] < -(*Sat)[0])
      (*inertialFrame)[0] = -(*Sat)[0];  

    if((*inertialFrame)[1] > (*Sat)[1])
      (*inertialFrame)[1] = (*Sat)[1];
    else if((*inertialFrame)[1] < -(*Sat)[1])
      (*inertialFrame)[1] = -(*Sat)[1];

    if((*inertialFrame)[2] > (*Sat)[2])
      (*inertialFrame)[2] = (*Sat)[2];
    else if((*inertialFrame)[2] < -(*Sat)[2])
      (*inertialFrame)[2] = -(*Sat)[2];

    //calculates error again for use in next iteration
    (*errorX)[1] = (targetPos[0] - currentPos[0]);
    (*errorY)[1] = (targetPos[1] - currentPos[1]);
    (*errorZ)[1] = (targetPos[2] - currentPos[2]);
    (*errorYaw)[1] = (targetPos[3] - targetPos[3]);

    if((*errorYaw)[1] > 180)
      (*errorYaw)[1] = ((*errorYaw)[1] - 360.0);
    else if ((*errorYaw)[1] < -180)
      (*errorYaw)[1] = ((*errorYaw)[1] + 360.0);

  }
  if(OC==false)
  {
    (*inertialFrame)[0] = 0.00;
    (*inertialFrame)[1] = 0.00;
    (*inertialFrame)[2] = 0.00;
    (*inertialFrame)[3] = 0.00;
  }
}


void Control::publishMessageControl(ros::Publisher *const publisher)
{
  geometry_msgs::Twist msg;
  msg.linear.x = inertialFrame->getX(); //Pitch
  msg.linear.y = inertialFrame->getY(); //Roll
  msg.linear.z = inertialFrame->getZ(); //Alt

  msg.angular.z = fixedFrame->getA();

  publisher->publish(msg);    
}

void Control::publishHoverControl(ros::Publisher *const publisher){
  geometry_msgs::Twist msg;
  msg.linear.x = inertialFrame->getX(); //Pitch
  msg.linear.y = inertialFrame->getY(); //Roll
  msg.linear.z = inertialFrame->getZ(); //Alt
  msg.angular.z = fixedFrame->getA();   //Yaw


  msg.angular.x = 0;
  msg.angular.y = 0;

  publisher->publish(msg);    
}
