/**
 @class     Control
 @brief     Provides control of UAVs
 @details   This class is the main form of control of the UAVs
 @author    Shawn Braune
 @author    David Mountafar
 @version   1.0
 @date      2014
 @pre       First initialize the ros system.

 @warning   Improper use can crash your application
 */

#ifndef Control_H
#define Control_h

#include "ros/ros.h"
#include <geometry_msgs/Twist.h>
#include "../Frame/Frame.h"
#include <math.h>
#include <stdio.h>
#include <time.h>
#include "../Mat3/mat3.h"
#include "../Vec3/vec3.h"
#include "../Vec4/Vec4.h"
#include "../Vec2/Vec2.h"

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 

#include <unistd.h>

class Control//: public CFrame
{
public:

  /** @brief Default Constructor

  This is the Default Constructor for the control class. Sets values to 
  a default value.

  @warning Use the constructor with params as it allows for better control
           over the values being implimented

  @author Shawn Braune
  @date October 2014
  */
  Control();

  /** @brief Constructor

  This is the Constructor for the control class. The user sets the values
  by passing in arguments

  @param std::string File path to control params file
  @param float Value of H for Euler. H is num/looprate.

  @author Shawn Braune
  @date October 2014
  */
  Control(std::string, float);


  /** @brief Destructor

  Dealocates memory and sets value to NULL

  @author Shawn Braune
  @date October 2014
  */
  ~Control();

  //Math Fucntions

  /** @brief Euler Integration

  Provides a method to perform Euler Integration

  @param float 
  @param float 

  @return

  @author Shawn Braune
  @author David Mountafar

  @date October 2014
  */
  float eulerIntegration(float,float);

  /** @brief Euler Derivative

  Provides a method to perform Euler Derivative

  @param float 
  @param float 

  @author Shawn Braune
  @author David Mountafar

  @date October 2014
  */
  float eulerDerivative(float, float);

  /** @brief Levant Derivative

  Provides a method to perform Euler Integration

  @param float 
  @param int Provides index for levant parameter

  @author Shawn Braune
  @author David Mountafar

  @date October 2014
  */
  float levantDerivative(float,int);

  /** @brief Get the Parameters From File

  Parses the input file and takes the control parameters values 
  and places it inside the array.

  @param float[16] Holds the control parameters
  @param std::string Control parameters file

  @author Shawn Braune

  @date January 2015
  */
  bool getParamsFromFile(float (&params)[16], std::string);

/*---------------------------UAV Control-------------------*/
  /** @brief Hover

  Sets hover for the UAV
  @author Shawn Braune

  @date October 2014
  */
  void hover();

  /** @brief Control

  Caluculated the new inertial frame for the UAV

  @param Vec4<float>& This is the new target position
  @param Vec4<float>& This is the position of the UAV
  @param bool Is a flag to allow the UAV to move or to hover

  @deprecated Use controlV2

  @author Shawn Braune
  @date January 2015
  */
  void control(Vec4<float>&, Vec4<float>&, bool);

  /** @brief Control Version 2

  An updated function to caluculated the new inertial frame 
  for the UAV

  @param Vec4<float>& This is the new target position
  @param Vec4<float>& This is the position of the UAV
  @param bool Is a flag to allow the UAV to move or to hover

  @deprecated Use controlV2

  @author Shawn Braune
  @date January 2015
  */
  void controlV2(Vec4<float>&, Vec4<float>&, bool);

  /** @brief Publish Message of Control

  Publishes inertial frame to the publisher

  @param ros::Publisher Is the ROS publisher

  @author Shawn Braune
  @date January 2015
  */
  void publishMessageControl(ros::Publisher *const); 

  /** @brief Publish Hover of Control

  Publishes an inertial frame of zero

  @param ros::Publisher Is the ROS publisher

  @author Shawn Braune
  @date January 2015
  */
  void publishHoverControl(ros::Publisher *const);
private:

  /**
  Is the constant for Euler
  */
  float *H; 
  
  /**
  Rotation Matrix
  */
  Mat3<float> *rotz;

  /**
  Error in X. First error using x, second is y,
  */   
  Vec2<float> *errorX;

  /**
  Derivitive of X using Euler(x), Levant(y),
  and integral of error(z)
  */
  Vec3<float> *dirvEurlLevIntErX; 

  /**
  Error in Y. First error using x, second is y,
  */ 
  Vec2<float> *errorY;

  /**
  Derivitive of Y using Euler(x), Levant(y),
  and integral of error(z)
  */
  Vec3<float> *dirvEurlLevIntErY; 

  /**
  Error in Z. First error using x, second is y,
  */ 
  Vec2<float> *errorZ;

  /**
  Derivitive of z using Euler(x), Levant(y),
  and integral of error(z)
  */
  Vec3<float> *dirvEurlLevIntErZ; 

  /**
  Error in Yaw. First error using x, second is y,
  */ 
  Vec2<float> *errorYaw;

  /**
  Derivitive of Yaw using Euler(x), Levant(y),
  and integral of error(z)
  */
  Vec3<float> *dirvEurlLevIntErYaw; 

  /**
  The inertial frame of the craft
  */
  Vec3<float> *inertialFrame;

  /**
  The fixed frame of the craft. Only Yaw is used for control
  */
  Vec4<float> *fixedFrame;
  
  /**
  Control Parameters for X
  */
  Vec3<float> *CP_X;

  /**
  Control Parameters for Y
  */
  Vec3<float> *CP_Y;

  /**
  Control Parameters for Z
  */
  Vec3<float> *CP_Z;

  /**
  Control Parameters for Yaw
  */
  Vec3<float> *CP_Yaw;

  /**
  Saturation values for X,Y,Z and Yaw
  */
  Vec4<float> *Sat;

  /**
  Array for Levant derivitive of X,Y,Z and Yaw
  */
  Vec4<float> levant[4];
  };
#endif
