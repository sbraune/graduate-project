/**
 @class     Waypoint 
 @brief     Waypoint Class for adding waypoints
 @details   This class allows you to add waypoints from a file or in
            in flight 

 @author    Shawn Braune
 @version   1.0
 @date March 2015
 @pre       First initialize the system.

 @warning   Improper use can crash your application
 */
#ifndef WAYPOINT_H
#define WAYPOINT_H

#include "../Vec4/Vec4.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib> 
#include <cassert>


#define DEBUG

 
class Waypoint { 

  public:

    /** @brief Default Constructor

    This is the Default Constructor for the Waypoint class. It parses the input file
    and places the waypoints into a std::vector.

    @param std::string Path to the waypoint file

    @author Shawn Braune
    @date March 2015
    */
    Waypoint(const std::string);

    /** @brief Default Deconstructor

    Deallocates memory and sets to null

    @author Shawn Braune
    @date March 2015
    */
    ~Waypoint();

    /** @brief Adding a Waypoint

    This allows you to add a waypoint to the waypoint class

    @param Vec4<float> 

    @author Shawn Braune
    @date March 2015
    */
    void addWaypoint(const Vec4<float>);

    /** @brief Adding a Waypoint

    This allows you to add a waypoint to the waypoint class

    @param float This is the X coordinate
    @param float This is the Y coordinate
    @param float This is the Z coordinate
    @param float This is the Yaw coordinate

    @author Shawn Braune
    @date March 2015
    */
    void addWaypoint(const float, const float, const float, const float);


    /** @brief Printing the contents of the waypoints

    This allows you to print all the waypoints within the waypoint class

    @author Shawn Braune
    @date March 2015
    */
    void print();

    /** @brief Get the number of waypoints

    This allows you to get the size of the std::vector in the waypoint class

    @returns int Size of the waypoint class

    @author Shawn Braune
    @date March 2015
    */
    int getSize();

    /** @brief Gets the current Waypoint

    This allows you to get the current Waypoint that is selected

    @returns Vec4<float> The Current Waypoint

    @author Shawn Braune
    @date March 2015
    */
    Vec4<float> getCurWaypoint();

    /** @brief Gets a waypoint 

    This allows you to get the waypoints of the index being passed

    @returns Vec4<float> The Current Waypoint

    @warning You need to make sure you are within the size of the vector
             Get the size to make sure you do not go out of bounds

    @author Shawn Braune
    @date March 2015
    */
    Vec4<float> getWaypoint(int);

    /** @brief Increments the Waypoint

    This allows you to increment to the next waypoint

    @author Shawn Braune
    @date March 2015
    */
    void increment();

    /** @brief Decrements the Waypoint

    This allows you to Decrement to the previous waypoint

    @author Shawn Braune
    @date March 2015
    */
    void decrement();

    /** @brief Gets the current index

    This allows you to get the current index of the Waypoint Class

    @author Shawn Braune
    @date March 2015
    */
    int getI();

private:

  /**The main storage for the Waypoints*/
  std::vector<Vec4<float> > *waypoint;
  /**The Size of the Waypoint container*/
  unsigned int *size;
  /**The current index*/
  unsigned int *cur;

};

#endif