#include "waypoint.h"

Waypoint::Waypoint(std::string fileName){

  int count = 0, innerCount = 0;
  float temp[4]; 
  std::string line;
  std::ifstream myfile( fileName.c_str());

  size = new unsigned int;
  cur = new unsigned int;
  waypoint = new std::vector<Vec4<float> >();
  *cur = 0;

  if (myfile){
    while (getline( myfile, line )){

      if(line[0] != '#'){
        std::istringstream ss( line );

        while (ss){

          std::string s;
          if (!getline( ss, s, ',' )) break;
            
            temp[innerCount] = atof(s.c_str());

            innerCount++;
          
        }

        Vec4<float> tmp = Vec4<float>(temp[0],temp[1],temp[2],temp[3]);
        tmp.print();
        waypoint->push_back(tmp);
 
          count++;
          if(innerCount == 4)
            innerCount = 0;
      }

    }

    myfile.close();
  }
  else std::cout << "Wrong File, please check if path is correct!!\n";
}

Waypoint::~Waypoint(){
  delete cur;
  delete waypoint;
}

Vec4<float> Waypoint::getCurWaypoint(){
  Vec4<float> temp = waypoint->at(*cur);
  return temp;
}

void Waypoint::addWaypoint(const Vec4<float> coord){
  waypoint->push_back(coord);
}

void Waypoint::addWaypoint(const float xCoord, const float yCoord, const float zCoord, const float yaw){
  Vec4<float> tmp(xCoord,yCoord,zCoord,yaw);

  waypoint->push_back(tmp);
}


Vec4<float> Waypoint::getWaypoint(int index){

  return waypoint->at(index);
}

void Waypoint::increment(){
  *cur+=1;

  if(*cur == waypoint->size())
    *cur = 0;
}

void Waypoint::decrement(){
  if(*cur == 0)
    *cur = waypoint->size();

  *cur-=1;
}

int Waypoint::getI(){
  return *cur;
}

int Waypoint::getSize(){
   return waypoint->size();
}

void Waypoint::print(){
  for(unsigned int i = 0; i < waypoint->size(); i++){
    if(waypoint->empty()){
      std::cout << "waypoint " << i+1 << " is null\n";
    }
    else{
      std::cout << i << " : "; 
      waypoint->at(i).print();
    }
  }
}