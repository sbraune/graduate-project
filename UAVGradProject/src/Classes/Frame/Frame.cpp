#include "Frame.h"
#include "math.h"

Frame::Frame(){
  //id
  frameID= new int;
  *frameID = 0;
  Battery= new float;
  *Battery = 0;
  translation = new Vec3<float>();
  rotation = new Vec3<float>();
  frameName = new std::string("UAV");


  translation->print();
} 

Frame::Frame(int id, std::string name){

  //id
  frameID= new int;
  *frameID = id;

  Battery= new float;
  *Battery = 0;
  frameName = new std::string(name);
  translation = new Vec3<float>();
  rotation = new Vec3<float>();

  translation->print();
  std::cout << frameName->size() << std::endl;

} 

Frame::~Frame(){
  delete translation;
  delete rotation;
  delete frameName;
  delete frameID;
  delete Battery;
}
  
void Frame::updatePosition(const geometry_msgs::Twist& msg){
  translation->set(msg.linear.x,msg.linear.y,msg.linear.z);
  rotation->set(msg.angular.x,msg.angular.y,msg.angular.z);

}

Vec3<float> Frame::getTranslation(){
  return Vec3<float>(translation->getX(),translation->getY(),translation->getZ());
}

Vec3<float> Frame::getRotation(){
  return Vec3<float>(rotation->getX(),rotation->getY(),rotation->getZ());
}

Vec4<float> Frame::getFlightPos(){
  return Vec4<float>(translation->getX(),translation->getY(),translation->getZ(),rotation->getZ());
}

float Frame::getXCord(){
  return translation->getX();
}

float Frame::getYCord(){
  return translation->getY();
}
float Frame::getZCord(){
  return translation->getZ();
}
float Frame::getPitch(){
  return rotation->getX();
}
float Frame::getRoll(){
  return rotation->getY();
}
float Frame::getYaw(){
  return rotation->getZ();
}
float Frame::getFuel(){
  return *Battery;
}


bool Frame::getRS(){
  return RS;
}

void Frame::updateBatteryStatus(const ardrone_autonomy::Navdata& msg){
 *Battery=msg.batteryPercent;
}

void Frame::printInfo(){
  ROS_INFO("%s Flight Info:", frameName->c_str());
  ROS_INFO("X=[%f],Y=[%f],Z=[%f]",
           translation->getX(),translation->getY(),translation->getZ());
  ROS_INFO("Roll=[%f],Pitch=[%f],Yaw=[%f]",rotation->getX(),rotation->getY(),rotation->getZ());
  ROS_INFO("Battery Percent=[%f]",*Battery);
}
