/**
 @class     Frame
 @brief     The physical frame of the UAV
 @details   Provides the main data for the physical frame of the UAV
 @author    Shawn Braune
 @version   1.0
 @date      2014
 @pre       First initialize the ros system.

 @warning   Improper use can crash your application
 */
#ifndef Frame_H
#define Frame_H

#include </home/tamucc/sbraune/UAV/vicon/msg_gen/cpp/include/vicon/position.h>
#include </home/tamucc/sbraune/UAV/ardrone_autonomy/msg_gen/cpp/include/ardrone_autonomy/Navdata.h>
#include "../Vec4/Vec4.h"
#include <geometry_msgs/Twist.h>
#include <iostream>
#include <string>


class Frame
{
public:

	/** @brief Default Constructor

  This is the Default Constructor for the frame class. Sets the frame name
  and frame ID to default.

  @warning Use the constructor with params as it allows for custome frame
           name and ID

  @author Shawn Braune
  @date October 2014
  */
	Frame();

	/** @brief Constructor

  This is the Constructor for the frame class. 

  @param int Frame ID
  @param std::string Name of the UAV

  @author Shawn Braune
  @date October 2014
  */
	Frame(int,std::string);
 	
  /** @brief Destructor

  Dealocates memory and sets value to NULL

  @author Shawn Braune
  @date October 2014
  */ 	
 	~Frame();

  /** @brief Print Info

  Prints the info of the Frame

  @author Shawn Braune
  @date October 2014
  */
 	void printInfo();

  /** @brief Update Position

  Updates the position of the frame.

  @author Shawn Braune
  @date October 2014
  */
 	void updatePosition(const geometry_msgs::Twist& msg);

  /** @brief Update Battery

  Updates the battery life of the frame

  @author Shawn Braune
  @date October 2014
  */
	void updateBatteryStatus(const ardrone_autonomy::Navdata& msg);

  /** @brief Get Translation of Frame

  This gets the Translation (X,Y and Z) of the frame 
  and returns it

  @returns Vec3<float>

  @author Shawn Braune
  @date October 2014
  */
	Vec3<float> getTranslation();

  /** @brief Get Rotation of Frame

  This gets the Rotation (Roll,Pitch and Yaw) of the frame 
  and returns it

  @returns Vec3<float>

  @author Shawn Braune
  @date October 2014
  */
	Vec3<float> getRotation();

  /** @brief Get Flight Position of Frame

  This gets the Flight Position (X,Y,Z and Yaw) of the frame 
  and returns it

  @returns Vec4<float>

  @author Shawn Braune
  @date October 2014
  */
	Vec4<float> getFlightPos();

  /** @brief Get X Coordinate

  This gets the X Coordinate of the frame 
  and returns it

  @returns float

  @author Shawn Braune
  @date October 2014
  */
	float getXCord();

  /** @brief Get Y Coordinate

  This gets the Y Coordinate of the frame 
  and returns it

  @returns float

  @author Shawn Braune
  @date October 2014
  */
	float getYCord();

  /** @brief Get Z Coordinate

  This gets the Z Coordinate of the frame 
  and returns it

  @returns float

  @author Shawn Braune
  @date October 2014
  */
	float getZCord();

  /** @brief Get Pitch Coordinate

  This gets the Pitch Coordinate of the frame 
  and returns it

  @returns float

  @author Shawn Braune
  @date October 2014
  */	
	float getPitch();

  /** @brief Get Roll Coordinate

  This gets the Roll Coordinate of the frame 
  and returns it

  @returns float

  @author Shawn Braune
  @date October 2014
  */
	float getRoll();

  /** @brief Get Yaw Coordinate

  This gets the Yaw Coordinate of the frame 
  and returns it

  @returns float

  @author Shawn Braune
  @date October 2014
  */
	float getYaw();

  /** @brief Get Remaining Fuel 

  This gets the Fuel Coordinate of the frame 
  and returns it

  @returns float

  @author Shawn Braune
  @date October 2014
  */
	float getFuel();

  /** @brief Get RS 

  This gets the RS of the frame 
  and returns it

  @returns float

  @author Shawn Braune
  @date October 2014
  */
	bool getRS();
	
	private:
		/** X,Y,Z Coordinates */
		Vec3<float> *translation;
		/**Roll,Pitch, Yaw Coordinates*/
		Vec3<float> *rotation;	
		/** ID of the Frame*/	
		int *frameID;
		/** Battery life or Fuel of the UAV*/
	  float *Battery;
	  /**Frame name of the UAV */
		std::string *frameName;
		/**RS Value of the UAV*/
		bool RS;



};
#endif
