/**
 @class     Vec4
 @brief     Vec4 Templated Class for UAV Systems
 @details   This class is a templated Vec4 class that is used in conjuction with 
            UAV Systems. It is inherited from the Vec4 class
 @see       Vec4
 @author    Shawn Braune
 @version   1.0
 @date      2014
 @pre       First initialize the system.

 @warning   Improper use can crash your application
 */
#ifndef VEC4_H
#define VEC4_H

#include <iostream>
#include <cassert>
#include "../Vec3/vec3.h"


template <class TYPE> 
class Vec4: public Vec3<TYPE> {
  
  public:
    /** @brief Default Constructor

    This is the Default Constructor for the Vec4 class. It initializes and sets
    all values to zero.

    @author Shawn Braune
    @date October 2014
    */
    Vec4();

    /** @brief Copy Constructor

    This is the Copy Consstructor for passing Vec4 by value or to make a copy of
    a Vec4. 

    @param Vec4<TYPE> Vector 4 to be copied and *Must Be of Vec4 Type*

    @author Shawn Braune
    @date October 2014
    */    
    Vec4(const Vec4<TYPE>&);

    /** @brief Copy Constructor

    This is the Copy Consstructor for passing Vec4 by value or to make a copy of
    a Vec4. 

    @param Vec3<TYPE> Vector 3 to be copied and *Must Be of Vec4 Type*.
    Sets the A value to 0.

    @author Shawn Braune
    @date October 2014
    */   
    Vec4(const Vec3<TYPE>&);

    /** @brief Copy Constructor

    This is the Copy Consstructor for passing Vec4 by value or to make a copy of
    a Vec3. 

    @param Vec3<TYPE> Vector 3 to be copied and *Must Be of Vec4 Type*
    @param TYPE A value for Vec4

    @author Shawn Braune
    @date October 2014
    */   
    Vec4(const Vec3<TYPE>&, const TYPE &aVal);
    
    /** @brief Constructor

    This Constructor can be used to intialize a contrsuctor with TYPE values. 

    @param TYPE Value for X and *Must Be of Vec4 Type*
    @param TYPE Value for Y and *Must Be of Vec4 Type*
    @param TYPE Value for Z and *Must Be of Vec4 Type*
    @param TYPE Value for A and *Must Be of Vec4 Type*

    @author Shawn Braune
    @date October 2014
    */    
    Vec4(const TYPE &xVal, const TYPE &yVal, const TYPE &zVal, const TYPE &aVal);

    /** @brief Destructor

    Dealocates memory and sets value to NULL

    @author Shawn Braune
    @date October 2014
    */
    ~Vec4();

    /** @brief Set X,Y,Z and A Values

    This sets the X,Y,Z and A cordinates respectivly. 

    @param TYPE Value for X and *Must Be of Vec4 Type*
    @param TYPE Value for Y and *Must Be of Vec4 Type*
    @param TYPE Value for Z and *Must Be of Vec4 Type*
    @param TYPE Value for A and *Must Be of Vec4 Type*

    @see setA
    @see Vec3

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec;

     TYPE xVal = 3.5,
          yVal = 2.2,
          zVal = .95,
          aVal = .05;

     vec.set(xval,yval,zval,aVal);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void set(const TYPE &xVal, const TYPE &yVal, const TYPE &zVal, const TYPE &aVal);
    
    /** @brief Set X,Y.Z and A Values

    This sets the X,Y,Z and A cordinates respectivly. 

    @param TYPE Value for X and *Must Be of Vec4 Type*
    @param TYPE Value for Y and *Must Be of Vec4 Type*
    @param TYPE Value for Z and *Must Be of Vec4 Type*
    @param TYPE Value for A and *Must Be of Vec4 Type*

    @see setA
    @see Vec3


    @b Example
    @code
     //Example using float Type

     Vec4<float> vec;
     Vec3<float> vec2(3.5,2.2,.95);

     TYPE aVal = 3.5;

     vec.set(vec2,aVal);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void set(const Vec3<TYPE>&, const TYPE &aVal);

    /** @brief Set A Value

    This sets the A value respectivly. 

    @param TYPE Value for Y and *Must Be of Vec4 Type*

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec;

     TYPE aVal = 2.2;

     vec.setA(aVal);
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void setA(const TYPE &val);


    /** @brief Get A

    This Gets the A value and returns the value of TYPE Vec4<TYPE>. 

    @returns TYPE Value for 4

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec;

     float aVal;

     aVal = vec.getA();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    TYPE getA() const;


    /** @brief Get the Vec4

    This Gets the Vector 4  and returns the value of TYPE Vec4<TYPE>. 

    @returns Vec4<TYPE>

    @see operator=

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec, newVec;

     newVec = vec.getvec4();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> getVec4() const;


    /** @brief Get Get the Vec3 Only

    This Gets the Vector 3 and returns the value of TYPE Vec4<TYPE>. 
     
    @returns Vec3<TYPE>


    @b Example
    @code
     //Example using float Type

     Vec3<float> vec;
     Vec4<float> vec2;

     vec2 = vec2.getVec3();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec3<TYPE> getVec3() const;

    //Helper

    /** @brief Print

    A helper function that prints the vector to screen

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec;

     vec.print();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void print();

   /** @brief Zero

    A helper function that sets the vector to zero

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec;

     vec.zero();
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void zero();

    //Overload Operators
    /** @brief Subtraction of two Vec3

    This is an overloaded addition operator that takes two Vec3<TYPE> and 
    subtractions them.

    @returns Vec3<TYPE> Subtracted Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2,vec3;

     vec3 = vec1 - vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> operator+(const Vec4<TYPE> &vec) const;

    /** @brief addition of two Vec4

    This is an overloaded addition operator that takes two Vec4<TYPE> and 
    adds them.

    @returns Vec4<TYPE> of Added Vector

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2,vec3;

     vec3 = vec1 + vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> operator=(const Vec4<TYPE> &vec) const;

    /** @brief Subtraction of two Vec4

    This is an overloaded addition operator that takes two Vec4<TYPE> and 
    subtractions them.

    @returns Vec4<TYPE> of Subtracted Vector

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2,vec3;

     vec3 = vec1 - vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> operator-(const Vec4<TYPE> &vec) const;

    /** @brief Multiplication of two Vec4

    This is an overloaded addition operator that takes two Vec4<TYPE> and 
    multiplies them.

    @returns Vec3<TYPE> Multiplied Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2,vec3;

     vec3 = vec1 * vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> operator*(const Vec4<TYPE> &vec) const;
    
    /** @brief Divide of two Vec3

    This is an overloaded addition operator that takes two Vec4<TYPE> and 
    divides them.

    @returns Vec4<TYPE> Divided Vector

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2,vec3;

     vec3 = vec1 / vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> operator/(const Vec4<TYPE> &vec) const;

    /** @brief Addition of a Vec3 and Scaler

    This is an overloaded divide operator that takes a Vec3<TYPE> and 
     a scaler and adds them together respectivily.

    @returns Vec3<TYPE> Added Vector

    @b Example
    @code
     //Example using float Type

     Vec3<float> vec1,vec2;

     vec2 = vec1 + 2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> operator+(const TYPE &val) const;

    /** @brief Subtraction of a Vec4 and Scaler

    This is an overloaded addition operator that takes a Vec4<TYPE> and 
     a scaler and subtracts them.

    @returns Vec4<TYPE> Subtracts Vector

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec2 = vec1 - 2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    Vec4<TYPE> operator-(const TYPE &val) const;

    /** @brief Multiplication of a Vec4 and Scaler

    This is an overloaded subtraction operator that takes a Vec4<TYPE> and 
     a scaler and multiplies them together respectivily.

    @returns Vec4<TYPE> Added Vector

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec2 = vec1 * 2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> operator*(const TYPE &val) const;

    /** @brief Division of a Vec4 and Scaler

    This is an overloaded multiplication operator that takes a Vec4<TYPE> and 
    a scaler and divdes them.

    @returns Vec4<TYPE> Added Vector

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec2 = vec1 / 2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    Vec4<TYPE> operator/(const TYPE &val) const;

    /** @brief Addition Equals

    This is an overloaded divde operator that takes a Vec4<TYPE> and
    adds them to Vec4 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1 += vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void operator+=(const Vec4<TYPE> &vec);

    /** @brief Subtraction Equals

    This is an overloaded addition equals operator that takes a Vec4<TYPE> and
    subtracts them from the Vec4 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1 -= vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void operator-=(const Vec4<TYPE> &vec);

    /** @brief Multiply Equals

    This is an overloaded subtraction equals operator that takes a Vec4<TYPE> and
    multiplies with the Vec4 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1 *= vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */  
    void operator*=(const Vec4<TYPE> &vec);

    /** @brief Division Equals

    This is an overloaded division equals operator that takes a Vec4<TYPE> and
    divdes it from the Vec4 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1 /= vec2;
    @endcode

    @author Shawn Braune
    @date October 2014
    */ 
    void operator/=(const Vec4<TYPE> &vec);

    /** @brief Addition Equals with Scaler

    This is an overloaded divide equals operator that takes a scaler and
    adds it to Vec4 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1 += 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void operator+=(const TYPE &val);

    /** @brief Subtraction Equals with Scaler

    This is an overloaded subtraction equals operator that takes a scaler and
    adds it to Vec4 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1 -= 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */  
    void operator-=(const TYPE &val);

    /** @brief Multiplication Equals with Scaler

    This is an overloaded multiplication equals operator that takes a scaler and
    adds it to Vec4 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1 *= 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */   
    void operator*=(const TYPE &val);

    /** @brief Divide Equals with Scaler

    This is an overloaded divide equals operator that takes a scaler and
    adds it to Vec4 on the left side.

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1 /= 3.0;
    @endcode

    @author Shawn Braune
    @date October 2014
    */ 
    void operator/=(const TYPE &val);

    /** @brief []

    This is an overloaded [] operator. It allows access to the values of 
    Vec4.

    @returns TYPE

    @b Example
    @code
     //Example using float Type

     Vec4<float> vec1,vec2;

     vec1[0] = vec2[2];
    @endcode

    @warning The program will not compile if you go beyond the size
             of the waypoint. It will throw an exception from cassert

    @author Shawn Braune
    @date March 2015
    */ 
    TYPE& operator[] (const int) ;

  protected:
    
    TYPE *a; /**A value for vector*/

};

#endif