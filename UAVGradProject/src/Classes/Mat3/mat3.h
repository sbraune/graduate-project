/**
 @class     Mat3
 @brief     Templated Mat3 Class for UAV Systems
 @details   This class is a templated 3x3 Matrix class that is used in conjuction with 
            UAV Systems
 @author    Shawn Braune
 @version   1.0
 @date      2014

 @warning   Improper use can crash your application
 */
#ifndef MAT3_H
#define MAT3_H

#include <iostream>
#include <math.h>
#include <cstddef>

#include "../Vec4/Vec4.h"

#define PI 3.14159265358979323846  /* pi */

template <class TYPE> 
class Mat3 {
  
  public:
    //Constructors and Destructors
    /** @brief Default Constructor

    This is the Default Constructor for the MAT3 class. It initializes and sets
    all values to zero.

    @author Shawn Braune
    @date October 2014
    */
    Mat3();

    /** @brief Copy Constructor

    This is the Copy Constructor for passing Mat3 by value or to make a copy of
    a Mat3

    @param Mat3<TYPE> 3x3 Matrix to be copied and
    @warning *Matrix Must Be of Mat3 Type*
    @author Shawn Braune
    @date October 2014
    */
    Mat3(const Mat3<TYPE>&);


    /** @brief Destructor

    Dealocates memory and sets to NULL

    @author Shawn Braune
    @date October 2014
    */
    ~Mat3();

    //Getters and Setters
    /** @brief Set Value

    This sets the value of the indicated row and column

    @param int Row number of the 3x3 matrix
    @param int Column number of 3x3 matrix
    @param TYPE Value to be set *Must Be of Vec3 Type*

    @see setRow()

    @return bool succession of Set

    @b Example
    @code
     //Example using float Type

     Mat3<float> mat;

     mat.setValue(0,    //Row
                  1,    //Col
                  3.5); //Value

    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void setValue(const int row, const int col, const TYPE &val);

    /** @brief Set Row

    This sets the indicated roll

    @param int Row number of the 3x3 matrix
    @param Vec3<TYPE> The Vec3 to be inserted *Must Be of Vec3 Type*

    @see setValue()

    @return bool succession of Set

    @b Example
    @code
     //Example using float Type

     Mat3<float> mat;
     Vec3<float> vec;

     mat.setRow(0,    //Row
                vec); //Values

    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void setRow(const int row, const Vec3<TYPE> &val);

    /** @brief Get Value

    This gets the value of the indicated row and column

    @param int Row number of the 3x3 matrix
    @param int Column number of 3x3 matrix
    @param TYPE Value to be get *Must Be of Vec3 Type*

    @see setRow()

    @return bool succession of Set

    @b Example
    @code
     //Example using float Type

     Mat3<float> mat;
     float val;

     mat.getValue(0,    //Row
                  1,    //Col
                  val); //Value

    @endcode

    @author Shawn Braune
    @date October 2014
    */    
    void getValue(const int row, const int col, TYPE &val) const;

    /** @brief Get Row

    This gets the indicated roll

    @param int Row number of the 3x3 matrix
    @param Vec3<TYPE> The Vec3 to get *Must Be of Vec3 Type*

    @see setValue()

    @return bool succession of Set

    @b Example
    @code
     //Example using float Type

     Mat3<float> mat;
     Vec3<float> vec;

     mat.getRow(0,    //Row
                vec); //Values

    @endcode

    @author Shawn Braune
    @date October 2014
    */
    void getRow(const int row, Vec3<TYPE> &val) const;

    //Helper
    /** @brief Zero out Matrix

    This zeros out the matrix by placing zeros in all
    the elements

    @author Shawn Braune
    @date October 2014
    */
    void zero();

    /** @brief Idenity

    This changes the matrix to the identity

    @author Shawn Braune
    @date October 2014
    */
    void identity();

    /** @brief Print the Values of the Matrix

    This prints the values with in the matrix

    @author Shawn Braune
    @date October 2014
    */
    void print();

    /** @brief Create the New Rotation

    This creates the new rotation of the matrix by taking the
    degrees and coverting it to radians and passing it to the
    rotate function

    @see rotate()

    @author Shawn Braune
    @date October 2014
    */
    void createRotation(const TYPE);

    //Overload Operators
    Vec3<TYPE> operator*(const Vec3<TYPE> &vec) const;
    Vec3<TYPE> operator*(const Vec4<TYPE> &vec) const;

  private:
    TYPE **mat; /**< TYPE pointer for Matrix */ 

    //helper
    /** @brief Rotate Matrix

    Rotates the matrix by the inputed value 

    @param TYPE Inputed value for rotation

    @see createRotation()

    @author Shawn Braune
    @date October 2014
    */
    void rotate(const TYPE);
};

#endif