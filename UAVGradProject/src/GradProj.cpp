/** @brief Graduate Project for Masters in Computer Science
  This is my Graduate Project for my Masters in Computer Science. 
  It uses ROS as it main backbone as long as several drivers that
  go along with ROS. In order to compile with ROS you need to make
  sure ROS knows where the package is by creating a new package
  and just copying the contents over.
  Dependence of the package: roscpp, sensor_msgs, std_msgs, std_srvs, tf

  After Compiling you need to run the following:
    roslaunch UAVGradProj Vicon.launch
    roslaunch UAVGradProj Drone1.launch
    roslaunch UAVGradProj Drone2.launch
    rosrun ARDroneGPS ARDroneGPS
    rosrun GlassNode Glassnode
    rosrun UAVGradProject UAVGradProj _logfile=:"Logfile Path" _looprate=:looprate

  @warning You will have to change the path name in the frame class in 
           order to reflect the path of the Vicon Server and the ARDrone
           Autonomy driver.

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
#include <stdio.h>      /* printf */
#include <iostream>
#include "./Classes/UAV/UAV.h"
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int16MultiArray.h"
#include <boost/thread.hpp>
#include <string>

std::string UAV_CMDS[2];

/** @brief Get UAV Command
  This function provides a means to get a UAV command from a subscription

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
 void getUAVCommand(const std_msgs::String::ConstPtr& msg)
{
  std::string cmd = msg->data.c_str();

    ROS_INFO("UAV #        : %c", cmd[0]);
    ROS_INFO("PRIMARY      : %c", cmd[1]);
    ROS_INFO("PRIM ALPHA   : %c", cmd[2]);
    ROS_INFO("2ndary       : %c", cmd[3]);
    ROS_INFO("2ndary ALPHA : %c", cmd[4]);
    ROS_INFO("CHECK SUM    : %c", cmd[5]);

    if(cmd[0] == '1'){
      UAV_CMDS[0] = cmd.substr(1,5);

      ROS_INFO("UAV1: %s", UAV_CMDS[0].c_str());
    }
    else if(cmd[0] == '2'){
      UAV_CMDS[1] = cmd.substr(1,5);

      ROS_INFO("UAV2: %s", UAV_CMDS[0].c_str());
    }
}

/*-----------------------------------------------------Main--------------------------------------------------------------*/
int main(int argc,char **argv)
{
  
  std::string logfile;
  int looprate = 200;
  int msgTimer = 0;
	
  ros::init(argc,argv, "UAVGradProject"); //create node
  ros::NodeHandle n; //Node Handle

  ros::NodeHandle private_node_handle_("~"); //Private Node handle

  //Get the LoopRate
  private_node_handle_.param("logfile", logfile, std::string("LogFile"));
  private_node_handle_.param("looprate", looprate, int(50));

  std::cout << logfile << std::endl;

  UAV_Params uav_params;
  UAV *uav[2];

  //Set Params and Create UAVs
  uav_params.set("ARDrone 1",
                   "/home/tamucc/sbraune/UAV/ARDroneV4/files/ARDroneV3.cpm",
                   "/ardrone1/ardrone/land",
                   "/ardrone1/ardrone/takeoff",
                   "/ardrone1/ardrone/reset",
                   "/ardrone1/cmd_vel",
                   "/ardrone1/ardrone/navdata",
                   "/ARDroneGPS/UAV1/Position",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/ARDrone.wpt",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/Pattern1.wpt",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/Pattern2.wpt",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/Pattern3.wpt",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/Pattern4.wpt",
                   logfile + "-UAV1",
                   looprate,
                   1);

  uav[0] = new UAV(uav_params);

  uav_params.set("ARDrone 2",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/ARDrone.cpm",
                   "/ardrone2/ardrone/land",
                   "/ardrone2/ardrone/takeoff",
                   "/ardrone2/ardrone/reset",
                   "/ardrone2/cmd_vel",
                   "/ardrone2/ardrone/navdata",
                   "/ARDroneGPS/UAV2/Position",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/ARDrone.wpt",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/Pattern1.wpt",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/Pattern2.wpt",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/Pattern3.wpt",
                   "/home/tamucc/sbraune/UAV/UAVGradProject/files/Pattern4.wpt",
                   logfile + "-UAV2",
                   looprate,
                   2);

  uav[1] = new UAV(uav_params);

  //Subscribers and publishers
  ros::Subscriber sub = n.subscribe("/GlassNode/cmd", 1000, getUAVCommand);
  ros::Publisher pub1 = n.advertise<std_msgs::Int16MultiArray>("/GlassNode/uav_data", 1000);

  
  ros::Rate rate(looprate); //cycle per seconds

  //Main Loop
  while(n.ok())
  {

  
    //UAV 1
    for(int i = 0; i < 2; i++){
      switch(UAV_CMDS[i][0]){

        //Take Off
        case '1': ROS_INFO("UAV %i: Taking OFF", i+1);
                  uav[i]->takeoff();
                  uav[i]->hover(800.0);
                  UAV_CMDS[i][0] = '3';
                  break;
        case '2': ROS_INFO("UAV %i: Landing", i+1);
                  uav[i]->land();
                  UAV_CMDS[i][0] = '0'; //Set to nothing
                  break;
        case '3': ROS_INFO("UAV %i: Hover", i+1);
                  uav[i]->hover();
                  break;
        case '4': ROS_INFO("UAV %i: Going to Waypoint %c",i+1, UAV_CMDS[i][1]);
                  uav[i]->gotoLoc((UAV_CMDS[i][1] - '0') - 1);
                  break;
        case '5': ROS_INFO("UAV %i: Doing Pattern %c",i+1, UAV_CMDS[i][1]);
                  uav[i]->pattern((UAV_CMDS[i][1] - '0'));
                  break;
        case '6': ROS_INFO("UAV %i: Resetting",i+1);
                  uav[i]->reset();
                  UAV_CMDS[i][0] = '0';
                  break;
     
    }

    msgTimer++;
    if(msgTimer > looprate*3){
      pub1.publish(uav[i]->getUAVData());
      msgTimer = 0; 
    }

  }
    ros::spinOnce();
    rate.sleep();
    
    
  }

  delete uav[0];
  delete uav[1];

  return 0;
}
