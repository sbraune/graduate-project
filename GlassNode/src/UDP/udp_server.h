/**
 @class     UDP_Server
 @brief     UDP Server for Coms
 @details   This is the class for use by the server in UDP
            communication
 @author    Shawn Braune
 @version   1.0
 @date      April 2015

 @warning   Improper use can crash your application and there 
            are no mechanisms in place for errors
 */
#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <arpa/inet.h>
#include <iostream>


class UDP_Server{

  public:
    /** @brief Default Constructor

    This is the Default Constructor for the UDP_Server Class

    @author Shawn Braune
    @date   April 2015
    */
    UDP_Server();
    /** @brief Constructor

    This is the a Constructor for the UDP_Server Class

    @params int port number of the server

    @author Shawn Braune
    @date Feb 2015
    */
    UDP_Server(int);

    /** @brief deconstructor

    This is the deconstructor for the UDP_Server Class

    @author Shawn Braune
    @date   April 2015
    */
    ~UDP_Server();

    /** @brief Recieve message from Client

    This is allows you to recieve a message to the client

    @params unsigned char* A Pointer ot the unsigned Char Buffer
    @params int Size of the buffer 

    @author Shawn Braune
    @date April 2015
    */
    int rcv_Message(unsigned char*, int size);

  private:
    int sock, /**Socket*/
    length; /**Length of Server*/
    struct sockaddr_in server; /**Sock Address Struct of Server*/
    struct sockaddr_in from; /**Sock Address Struct of Client*/
    socklen_t fromLen; /**Sock length of the client*/

};
#endif