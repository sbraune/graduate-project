#include "udp_client.h"

UDP_Client::UDP_Client(){
  sock = socket(AF_INET, SOCK_DGRAM,0);
  server.sin_family = AF_INET;
  server.sin_port = htons(54000);
  server.sin_addr.s_addr = inet_addr("192.168.2.24");

  server_len = sizeof(server);
  client_len = sizeof(client);
}

UDP_Client::UDP_Client(std::string ipAddr, int port){
  sock = socket(AF_INET, SOCK_DGRAM,0);
  server.sin_family = AF_INET;
  server.sin_port = htons(port);
  server.sin_addr.s_addr = inet_addr(ipAddr.c_str());

  server_len = sizeof(server);
  client_len = sizeof(client);
}

UDP_Client::~UDP_Client(){}

void UDP_Client::send_Message(unsigned char *buffer, int bufSize){

  sendto(sock, buffer, bufSize, 0, (struct sockaddr *) &server, server_len);
}