#include "udp_server.h"

UDP_Server::UDP_Server(){

  sock=socket(AF_INET, SOCK_DGRAM, 0);

  if(sock < 0){
    std::cout << "Error: Opening Socket\n";
  }

  length = sizeof(server);
  bzero(&server,length);

  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(53000);

  if (bind(sock, (struct sockaddr *)&server, length) < 0){
     std::cout << "Error: Binding\n";  
  }

  fromLen = sizeof(struct sockaddr_in);
}

UDP_Server::UDP_Server(int port){

  sock=socket(AF_INET, SOCK_DGRAM, 0);

  if(sock < 0){
    std::cout << "Error: Opening Socket\n";
  }

  length = sizeof(server);
  bzero(&server,length);

  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons(port);

  if (bind(sock, (struct sockaddr *)&server, length) < 0){
     std::cout << "Error: Binding\n"; 
  }

  fromLen = sizeof(struct sockaddr_in);
}

UDP_Server::~UDP_Server(){}

int UDP_Server::rcv_Message(unsigned char *buffer, int size){
 return recvfrom(sock,buffer,size,0,(struct sockaddr *) &from, &fromLen);
}

