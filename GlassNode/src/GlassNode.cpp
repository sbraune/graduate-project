/** @brief Glass Node
  
  This is the node that recieves the users input and publishes it
  and sends the UAV data back to the User. Originally to work in
  conjunction with the Google Glass. It still has that capability 
  as long as the the glass is sending the correct message.

  After Compiling you need to run the following:
    rosrun GlassNode Glassnode

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int16MultiArray.h"
#include <sstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <arpa/inet.h>
#include <iostream>

#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>

#include "UDP/udp_server.h"
#include "UDP/udp_client.h"


int count = 0;
bool finished = false;

void send_Message(const std_msgs::Int16MultiArray array);
void threadGetMSG(ros::Publisher &publisher);


int main(int argc, char **argv){


  ros::init(argc, argv, "GlassNode");


  ros::NodeHandle node;
  
  //Create Publisher
  ros::Publisher pub = node.advertise<std_msgs::String>("/GlassNode/cmd", 1000);

  //creating Subscriber and subscribe
  ros::Subscriber sub1 = node.subscribe("/GlassNode/uav_data", 1000, send_Message);


  ros::Rate loop_rate(10); //not used but needed for ROS
  
  //Create thread and begin work
  boost::thread msgThread(threadGetMSG, pub);

  while(ros::ok()){

    ros::spinOnce();
    loop_rate.sleep();
    count++;
  }

  return 0;
}

/** @brief Getting Message in Thread
  This function works in conjunction with boost threads to get messages
  from the user in a parallel fassion

  @warning This should be updated to use int arrays and not a string

  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
void threadGetMSG(ros::Publisher &publisher){
  UDP_Server server;
  unsigned char buf[12];


  std::cout << "in Message Thread\n";

  int checksum = 0, rcv_checksum;

  while(finished == false){

    server.rcv_Message(buf, 12); //Wait for a message

    ROS_INFO("UAV #        : %i", (buf[0] | buf[1] << 8));
    ROS_INFO("PRIMARY      : %i", (buf[2] | buf[3] << 8));
    ROS_INFO("PRIM ALPHA   : %i", (buf[4] | buf[5] << 8));
    ROS_INFO("2ndary       : %i", (buf[6] | buf[7] << 8));
    ROS_INFO("2ndary ALPHA : %i", (buf[8] | buf[9] << 8));
    ROS_INFO("CHECK SUM    : %i\n", (buf[10] | buf[11] << 8));
                              

    std_msgs::String msg;

    std::stringstream ss;

    for(int i = 0; i < 6; i++){
      ss << (buf[i*2] | buf[(i*2)+1] << 8);

      i < 5 ? checksum += (buf[i*2] | buf[(i*2)+1] << 8) : rcv_checksum = (buf[i*2] | buf[(i*2)+1] << 8);
    }
 
    msg.data = ss.str();

    ROS_INFO("%s, RCV_Checksum: %i Checksum: %i", msg.data.c_str(), rcv_checksum,checksum);

    //Checking Checksum
    if(rcv_checksum == checksum){
      ROS_INFO("Checksums match, sending msg RCV: %i, Calc: %i", rcv_checksum, checksum);
      publisher.publish(msg);
    }
    else
      ROS_INFO("Checksums do not match RCV: %i, Calc: %i", rcv_checksum, checksum);

    checksum = 0;
  }
}

/** @brief Send Message to User
  This function sends a message to the user once it sees their is a 
  message in the publishing queue

  @warning Saturation needs to change depending on size of room
  @author    Shawn Braune
  @version   1.0
  @date      Spring 2015
 */
void send_Message(const std_msgs::Int16MultiArray array)
{
  unsigned char buf[20];

  int sat = 20000; //saturation
  UDP_Client client;

  //Split and place in buffer
  buf[0] = (array.data[0]) &0xFF;
  buf[1] = ((array.data[0]) >> 8) &0xFF;

  buf[2] = (array.data[1]+sat) &0xFF;
  buf[3] = ((array.data[1]+sat) >> 8) &0xFF;

  buf[4] = (array.data[2]+sat) &0xFF;
  buf[5] = ((array.data[2]+sat) >> 8) &0xFF;

  buf[6] = (array.data[3]+sat) &0xFF;
  buf[7] = ((array.data[3]+sat) >> 8) &0xFF;

  buf[8] = (array.data[4]+sat) &0xFF;
  buf[9] = ((array.data[4]+sat) >> 8) &0xFF;

  buf[10] = (array.data[5]+sat) &0xFF;
  buf[11] = ((array.data[5]+sat) >> 8) &0xFF;

  buf[12] = (array.data[6]+sat) &0xFF;
  buf[13] = ((array.data[6]+sat) >> 8) &0xFF;

  buf[14] = (array.data[7]) &0xFF;
  buf[15] = ((array.data[7]) >> 8) &0xFF;

  buf[16] = (array.data[8]) &0xFF;
  buf[17] = ((array.data[8]) >> 8) &0xFF;

  buf[18] = (array.data[9]) &0xFF;
  buf[19] = ((array.data[9]) >> 8) &0xFF;


  client.send_Message(buf, 20);


  //print out command
  std::cout << (buf[0] | buf[1] << 8) << " ";
  std::cout << (buf[2] | buf[3] << 8) - sat << " ";
  std::cout << (buf[4] | buf[5] << 8) - sat << " ";
  std::cout << (buf[6] | buf[7] << 8) - sat << " ";
  std::cout << (buf[8] | buf[9] << 8) - sat << " ";
  std::cout << (buf[10] | buf[11] << 8) - sat << " ";
  std::cout << (buf[12] | buf[13] << 8) - sat << " ";
  std::cout << (buf[14] | buf[15] << 8) << " ";
  std::cout << (buf[16] | buf[17] << 8) << " ";
  std::cout << (buf[18] | buf[19] << 8) << "\n";

}